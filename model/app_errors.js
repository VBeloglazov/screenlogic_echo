'use strict';

var debug = require('debug')('screenlogic-echo:app-errors');
var Sequelize = require('sequelize');
var cfg = require('../config');

/**
 * Adapter constructor function
 *
 * @constructor
 */
var Adapter = module.exports = function () {

    if (!(this instanceof Adapter)) {return new Adapter(); }

    this.config = cfg.lockit;

    // create connection string
    var uri = this.config.db.url + this.config.db.name;
    var sequelize = new Sequelize(uri, {
        storage: this.config.db.name
    });

    this.AppError = sequelize.define('AppError', {
        // make id like CouchDB and MongoDB
        _id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        date: {
            type: Sequelize.DATE,
        },
        mail: {
            type: Sequelize.STRING
        },
        device_id: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.TEXT
        },
        options: {
            type: Sequelize.TEXT
        },
        response: {
            type: Sequelize.TEXT
        },
        status: {
            type: Sequelize.STRING
        },
        user_id: {
            type: Sequelize.TEXT
        }
    }, {
        tableName: 'app_errors',   // this will define the table's name
        timestamps: false                 // this will deactivate the timestamp columns
    });

    this.AppError.sequelize.sync({}).then(() => {
        // you can now use Device to create new instances
        debug('OauthClient:sync: %s', 'OK');
        return null;
    })
        .error(err => {
            throw err;
        });
};

Adapter.prototype.addError = function(mail = "", device_id = "", url = "", options = "", response = "", status = "", user_id= "") {
    const query = {
        date: new Date().toUTCString(),
        mail,
        device_id,
        url,
        options,
        response,
        status,
        user_id
    };

    const error = this.AppError.build(query);

    return error.save()
        .catch(function(saveErr) {
            debug('addDevice:error: %s', JSON.stringify(saveErr));
        });
};

Adapter.prototype.findAll = function(query) {
    return this.AppError.findAll(query);
};
