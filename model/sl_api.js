/* global Promise */

'use strict';

const cfg = require('../config');
const url = require('url');
const querystring = require('querystring');
var user_db = require('./user')(cfg.lockit);

var parsedApiUrl = url.parse(cfg.slapi.host);
const http = require(parsedApiUrl.protocol==="https:"?'https':'http');

const debug = require('debug')('screenlogic-echo:sl_api');
//const plogger = require('./plogger');
const AppErrorsRecorder = require('./app_errors')(cfg.lockit);

const NodeCache = require( "node-cache" );
const myCache = new NodeCache( { stdTTL: 100, checkperiod: 120 } );

const NS_PER_SEC = 1e9;


/**
 * Build http.request specific options
 *
 * @param {String} name - request name
 * @param {Object} data - request data
 * @returns {Object} options - {hostname: 'www.google.com', port: 80, path: '/upload', method: 'POST', headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': 35}}
 */
function buildHttpRequestOptions (name, data) {
    debug(`${name}:data: %s`, JSON.stringify(data));

    const config = cfg.slapi;

    // build full url for request
    let fullUrl = `${config.host}/${name}`;

    // add params if available
    if (data.params) {
        fullUrl = `${fullUrl}?${querystring.stringify(data.params)}`;

    }
    debug(`${name}:URL built: %s`, fullUrl);

    // create options from url
    const opts = url.parse(fullUrl);

    // general headers
    opts.headers = {
        'X-AppVersion': config.appVersion,
        'X-DeviceID': `SLEcho_${data.user_id}`
    };

    // set session header if available
    if (data.session_id) {
        opts.headers['X-Session'] = data.session_id;
    }

    // set POST params, only POST is /login
    if (name === 'login') {
        opts.method = 'POST';

        opts.headers['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

        const postData = querystring.stringify({
            loginName: data.dev_login,
            pass: data.dev_pwd
        });
        opts.headers['Content-Length'] = Buffer.byteLength(postData);

        // wait more requests after login, keep connection
        opts.headers.Connection = 'keep-alive';
    }

    debug(`${name}:OPTIONS built: %s`, JSON.stringify(opts));
    return opts;
}


/**
 * Make request to API, get response data and check for errors
 *
 * @param {String} name - request name
 * @param {Object} data - request data
 * @returns {Promise} - promise for callAPI
 */
function callAPI (name, data) {
    return new Promise(async (resolve, reject) => {
        const options = buildHttpRequestOptions(name, data);
        let postData = '';
        let plogger_data = {};
        if (name === 'login') {
            const preparedLoginName =`Pentair: ${data.dev_login.split(" ")[1].toUpperCase()}`;
            postData = querystring.stringify({loginName: preparedLoginName, pass: data.dev_pwd});
            debug(`${name}:API post: %s`, postData);
        }

        var user;

        if (data.user_id) {
            user = await user_db.find('_id', data.user_id);
        } else {
            user = { email: data.session_id };
        }

        var hrstart = process.hrtime();

        const req = http.request(options, res => {
            console.error(`${name}:status: %s`, res.statusCode);
            plogger_data = {
                uid:data.user_id,
                dev_id:data.dev_login?data.dev_login:(myCache.get( "devNameBySes-"+data.session_id )||""),
                session_id:data.session_id?data.session_id:"",
                req_href:options.href,
                req_name:name,
                req_body:postData,
                req_headers:JSON.stringify(options.headers),
                req_method:options.method?options.method:'GET',
                statuscode:res.statusCode,
                res_headers:JSON.stringify(res.headers),
                dt:new Date().toUTCString()
            };

            res.setEncoding('utf8');

            // collect full body from chunks
            let rawBody = '';
            let chunkCnt = 0;

            res.on('data', chunk => {
                chunkCnt += 1;
                rawBody += chunk;
                debug(`${name}:got BODY chunk: %s`, chunkCnt);
            });

            res.on('end', () => {
                debug(`${name}:request:end: No more data in response.`);
                const diffTime = process.hrtime(hrstart);
                plogger_data.res_time = Math.round((diffTime[0] * NS_PER_SEC + diffTime[1])/1000000);
                plogger_data.res_body = rawBody;
                //plogger.log(plogger_data);
                console.error(plogger_data);

                // NOTE: poolButtonPress return nothing on success
                if ( res.statusCode === 200 && !rawBody) { //(name === 'poolButtonPress' || name === 'poolSetHeatMode') &&
                    debug(`${name}:return nothing: %s`, JSON.stringify(rawBody));
                    return resolve({});
                }

                // other queries must return data
                try {
                    const body = JSON.parse(rawBody);
                    debug(`${name}:BODY: %s`, JSON.stringify(body));
                    console.error(JSON.stringify(body));

                    if (name === 'login' && body.sessionId > 0) {
                        myCache.set( "devNameBySes-"+body.sessionId, data.dev_login );
                    }

                    if (body && body.errorMessage) {
                        AppErrorsRecorder.addError(user.email, data.dev_login||data.device_id, options.href, JSON.stringify(postData), body.errorMessage, res.statusCode, data.user_id);
                        return reject(body);
                    }

                    if (res.statusCode !== 200) {
                        AppErrorsRecorder.addError(user.email, data.dev_login||data.device_id, options.href, JSON.stringify(postData), res, res.statusCode, data.user_id);
                        return reject(`Connection error code: ${res.statusCode}`);
                    }

                    resolve(body);
                } catch (e) {
                    console.error(new Error(e));
                    reject(e);
                }
            });
        });

        req.on('error', e => {
            if(e.code !== "HPE_INVALID_CONSTANT") {
                AppErrorsRecorder.addError(user.email, data.dev_login||data.device_id, options.href, postData, e, e.code, data.user_id);
                plogger_data.res_body = e;
                //plogger.log(plogger_data);
                debug(`${name}:API request:error: %s`, e.message);
                console.error(plogger_data);
                reject(e.message);
            }
        });

        // write data to request body
        req.write(postData);
        req.end();
    })
    .catch(err => {
        debug(`${name}:catched: %s`, JSON.stringify(err));
        console.error(JSON.stringify(err));

        throw err.message || err;
    });
}


/**
 * Login
 *
 * @param {Number} user_id - registered user id
 * @param {String} dev_login - pool device login
 * @param {String} dev_pwd - pool device password
 * @returns {Promise} - promise for callAPI
 * @example {"sessionId": 7}
 */
function slLogin (user_id, dev_login, dev_pwd) {
    return callAPI('login', {user_id, dev_login, dev_pwd});
}


/**
 * Get Pool Data
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @returns {Promise} - promise for callAPI
 * @example {"poolData":{"name":"Pentair: E9-F4-DB","version":"POOL: 5.2 Build 733.0 Rel","zip":"H4Y1H1",
 *           "latitude":34,"longitude":119,"timeZone":-8,"weatherType":1,"weatherURL":""}}
 */
function slGetPoolData (user_id, session_id, dev_login) {
    return callAPI('poolData', {user_id, session_id, dev_login});
}


/**
 * Get Pool Config Data
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @returns {Promise} - promise for callAPI
 */
function slGetPoolConfigData (user_id, session_id, dev_login) {
    return callAPI('poolCtrlConfig', {user_id, session_id, dev_login});
}


/**
 * Get Pools Status
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @returns {Promise} - promise for callAPI
 */
function slGetPoolsStatus (user_id, session_id, dev_login) {
    return callAPI('poolStatus', {user_id, session_id, dev_login});
}


/**
 * Get chem data
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @returns {Promise} - promise for callAPI
 */
function slGetChemData (user_id, session_id, dev_login) {
    return callAPI('poolChemData', {user_id, session_id, dev_login});
}

/**
 * Get all chem data 
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @returns {Promise} - promise for callAPI
 */
function slGetChemDataAll (user_id, session_id, dev_login) {
    return callAPI('poolGetChemDataAll', {user_id, session_id, dev_login});
}


/**
 * Get salt config
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @param {Object} params - {"ctrlIndex":@0};
 * @returns {Promise} - promise for callAPI
 */
function slGetSaltConfig (user_id, session_id, params, dev_login) {
    return callAPI('poolGetScgConfig', {user_id, session_id, params, dev_login});
}

/**
 * Set salt config
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @param {Object} params - {"progressOutput1":m_chlorPool, "progressOutput2":m_chlorSpa, "bSuper":b_Super, "iSuper":i_Super};
 * @returns {Promise} - promise for callAPI
 */
function slSetSaltConfig (user_id, session_id, params, dev_login) {
    return callAPI('poolSetScgConfig', {user_id, session_id, params, dev_login});
}


/**
 * Set Body Heater
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @param {Object} params - {"ctrlIndex":@0, "bodyType":iBodyType, @"heatSp":m_iSetPoint};
 * @returns {Promise} - promise for callAPI
 */
function slSetHeatSP (user_id, session_id, params, dev_login) {
    return callAPI('poolSetHeatSp', {user_id, session_id, params, dev_login});
}


/**
 * Set circuit on/off
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @param {Object} params - {"ctrlIndex":@0, "circuitId":m_iCircuitID, @"isOn":m_iFlags}
 * @returns {Promise} - promise for callAPI
 */
function slSetCircuitOnOff (user_id, session_id, params, dev_login) {
    return callAPI('poolButtonPress', {user_id, session_id, params, dev_login});
}


/**
 * Set heat mode
 *
 * @param {Number} user_id - registered user id
 * @param {Number} session_id - current api session
 * @param {Object} params - {@"ctrlIndex":@0, @"bodyType":iBodyType, @"heatMode":m_iHeatMode}
 * @returns {Promise} - promise for callAPI
 */
function slSetHeatMode (user_id, session_id, params, dev_login) {
    return callAPI('poolSetHeatMode', {user_id, session_id, params, dev_login});
}


/**
 * Get all available data at once
 *
 * @param {Number} user_id - registered user id
 * @param {String} dev_login - pool device login
 * @param {String} dev_pwd - pool device password
 * @returns {Promise} - promise for array of pool data
 * @example [{poolData},{poolStatus},{poolCtrlConfig},{chemData}]
 */
function slGetAllData (user_id, dev_login, dev_pwd) {
    return callAPI('login', {user_id, dev_login, dev_pwd})
        .then(body => {
            return Promise.all([
                Promise.resolve().then(() => body),  // store sessionId
                slGetPoolData(user_id, body.sessionId, dev_login),
                slGetPoolsStatus(user_id, body.sessionId, dev_login),
                slGetPoolConfigData(user_id, body.sessionId, dev_login),
                slGetChemDataAll(user_id, body.sessionId, dev_login),
                slGetSaltConfig(user_id, body.sessionId,  {ctrlIndex: 0}, dev_login)
            ])
            .then(data => {
                const ret = {};
                for (const apiCall of data) {
                    for (const p in apiCall) {
                        ret[p] = apiCall[p];
                    }
                }
                return ret;
            });
        });
}


module.exports = {
    slLogin,
    slGetPoolData,
    slGetChemData,
    slGetChemDataAll,
    slGetPoolConfigData,
    slGetPoolsStatus,
    slGetAllData,
    slSetHeatSP,
    slSetCircuitOnOff,
    slSetHeatMode,
    slGetSaltConfig,
    slSetSaltConfig
};
