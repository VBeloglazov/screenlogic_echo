'use strict';

var debug = require('debug')('screenlogic-echo:device-model');
var Sequelize = require('sequelize');
var cfg = require('../config');

/**
 * Adapter constructor function
 *
 * @constructor
 */
var Adapter = module.exports = function () {

    if (!(this instanceof Adapter)) {return new Adapter(); }

    this.config = cfg.lockit;

    // create connection string
    var uri = this.config.db.url + this.config.db.name;
    var sequelize = new Sequelize(uri, {
        storage: this.config.db.name
    });

    this.Device = sequelize.define('Device', {
        // make id like CouchDB and MongoDB
        _id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id: {
            type: Sequelize.INTEGER,
            unique: 'devices_pkey',
            validate: {
                notEmpty: true,
                isInt: true
            }
        },
        alias: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: true,
                // is: ["^[a-z ]+$",'i']
            }
        },
        device_id: {
            type: Sequelize.STRING,
            unique: 'devices_pkey',
            validate: {
                notEmpty: true
            }
        },
        password: {
            type: Sequelize.STRING,
            validate: {
                //notEmpty: true  // TODO: disabled for test devices
            }
        },
        enabled: Sequelize.BOOLEAN,
        emails: Sequelize.STRING
    }, {
        tableName: 'pool_devices',   // this will define the table's name
        timestamps: false                 // this will deactivate the timestamp columns
    });

    this.Device.sequelize.sync({}).then(() => {
        // you can now use Device to create new instances
        debug('OauthClient:sync: %s', 'OK');
        return null;
    })
    .error(err => {
        throw err;
    });
};


/**
 * Add new pool device to db.
 *
 * @param {String} user_id - Device alias
 * @param {String} dev_alias - Device alias
 * @param {String} dev_id - Device id
 * @param {String} dev_pwd - Plain text password
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.addDevice = function(user_id, dev_alias, dev_id, dev_pwd, done) {
    var that = this;

    var query = {
        user_id: user_id,
        alias: dev_alias,
        device_id: dev_id,
        password: dev_pwd,
        enabled: true
    };
    debug('addDevice:query: %s', JSON.stringify(query));

    var device = that.Device.build(query);

    // save to db
    return device.save()
        .then(function(ret) {
            debug('addDevice:type: %s', JSON.stringify(ret));
            done(null);
        })
        .error(function(saveErr) {
            debug('addDevice:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('addDevice:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'You can\'t add this device.';
            done(err);
            //throw (err);
        });
};


/**
 * Import new pools device to db.
 *
 * @param {Number} user_id - Device alias
 * @param {Array} data - Devices
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.importDevices = function(user_id, data, done) {
    var that = this;

    debug('importDevices:length: %d', data.length);

    data2save = [];
    data.forEach(function(entry) {
        data2save.push({
            user_id: user_id,
            alias: entry.alias,
            device_id: entry.device_id,
            password: entry.pass,
            enabled: true,
            emails: entry.emails?JSON.stringify(entry.emails):''
        });
    });

    
    return that.Device.bulkCreate(data2save)
        .then(function(ret) {
            debug('importDevices:type: %s', JSON.stringify(ret));
            return ret
        })
        .error(function(saveErr) {
            debug('importDevices:error: %s', JSON.stringify(saveErr));
            throw saveErr;
        })
        .catch(function (err) {
            debug('importDevices:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'You can\'t add this device.';
            throw (err);
        });
};


/**
 * Edit pool device to db.
 *
 * @param {Number} id - Device id in db
 * @param {Number} user_id - Device alias
 * @param {String} dev_alias - Device alias
 * @param {String} dev_id - Device id
 * @param {String} dev_pwd - Plain text password
 * @param {Boolean} enabled - enabled for alexa access
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.updateDevice = function(id, user_id, dev_alias, dev_id, dev_pwd, enabled, done) {
    var that = this;

    var query = {
        alias: dev_alias,
        device_id: dev_id,
        password: dev_pwd,
        enabled: enabled
    };
    debug('updateDevice:query: %s', JSON.stringify(query));

    // save to db
    return that.Device.update(query, {where: {_id: id, user_id: user_id}})
        .then(function(ret) {
            debug('updateDevice:type: %s', JSON.stringify(ret));
            done(null);
        })
        .error(function(saveErr) {
            debug('updateDevice:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('updateDevice:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'Some values you set is wrong.';
            done(err);
            //throw (err);
        });
};


/**
 * Find all user devices
 *
 * @param user_id
 * @param {Function} done
 * @returns {*}
 */
Adapter.prototype.getUserDevices = function (user_id) {
    const query = {user_id};
    debug('getUserDevices:query: %s', JSON.stringify(query));

    // get devices
    return this.Device.findAll({where: query}).then(devices => {
        debug('getUserDevices:result: %s', JSON.stringify(devices));
        devices = devices || {};
        return devices;
    })
    .error(err => {
        debug('getUserDevices:error: %s', JSON.stringify(err));
        throw err;
    })
    .catch(e => {
        debug('getUserDevices:catched: %s', JSON.stringify(e));
        throw e;
    });
};


/**
 * Find user device by id
 *
 * @param {Number} device_id
 * @param {Function} done
 * @returns {*}
 */
Adapter.prototype.getDeviceById = function(device_id, done) {
    var that = this;

    var query = {
        _id: device_id
    };


    console.error('getDeviceById:query: %s', JSON.stringify(query));

    // get devices
    return that.Device.find({where: query})
        .then(function(device) {
            console.error('getDeviceById:result: %s', JSON.stringify(device));
            device = device || {};
            done(null, device);
        })
        .error(function(saveErr) {
            console.error('getDeviceById:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            console.error('getDeviceById:catched: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};


Adapter.prototype.getUserDeviceById = function(user_id, device_id, done) {
    var that = this;

        var query = {
            device_id: device_id,
            user_id: user_id,
        };


    debug('getUserDeviceById:query: %s', JSON.stringify(query));

    // get devices
    return that.Device.find({where: query})
        .then(function(device) {
            debug('getUserDeviceById:result: %s', JSON.stringify(device));
            device = device || {};
            done(null, device);
        })
        .error(function(saveErr) {
            debug('getUserDeviceById:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('getUserDeviceById:catched: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};


/**
 * Remove an existing user device from db.
 *
 * @param {Number} user_id - user id
 * @param {String} _id - Device id in db
 * @param {Function} done - Callback function having `err` and `res` arguments
 */
Adapter.prototype.delDevice = function(user_id, _id, done) {
    var that = this;

    var query = {
        user_id: user_id,
        _id: _id
    };
    debug('delDevice:query: %s', JSON.stringify(query));

    // find
    return that.Device.find({ where: query })
        .then(function(device) {
            if (!device) {return done(new Error('Error: Cannot find this device in your list')); }
            return device.destroy()
                .then(function() {
                    debug('delDevice:result: %s', 'deleted');
                    done(null, true);
                })
                .error(function(err) {
                    debug('delDevice:error: %s', JSON.stringify(err));
                    done(err);
                })
                .catch(function (err) {
                    debug('delDevice:catched destroy: %s', JSON.stringify(err));
                    //done(err);
                    throw (err);
                });
        })
        .error(function(err) {
            debug('delDevice:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('delDevice:catched find: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};
