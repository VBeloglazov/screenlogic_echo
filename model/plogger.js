'use strict';
var net = require('net');

var logHost = '148.251.180.148'
    , logPort = 9563;

/**
 * 
 * @param {*} params 
 */
function log(params) {
    // console.log("----------------------------------");
    // console.log(params);
    // console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

    let log_message = params.uid+' '+params.dev_id+' '+params.req_method+' '+params.req_name+' '+params.statuscode;
    var conn = net.createConnection({ host: logHost, port: logPort }, function () {
        var message = {
            '@tags': ['pentairpoolchat.com', 'test']
            , '@message': log_message
            , '@fields':  params
        }
        conn.end(JSON.stringify(message));
    })
        .on('error', function (err) {
            console.error(err);
            // process.exit(1);
        });
}

// var mysql = require('mysql');
// var connection = mysql.createConnection({
//     host: 'localhost',
//     user: 'me',
//     password: 'secret',
//     database: 'my_db'
// });

// connection.connect();

// connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//   if (error) throw error;
//   console.log('The solution is: ', results[0].solution);
// });

// connection.end();



module.exports = {
    log,
};