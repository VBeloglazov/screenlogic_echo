'use strict';

/**
 * Module dependencies.
 */
const debug = require('debug')('screenlogic-echo:oauth-model');
const Sequelize = require('sequelize');


/**
 * Adapter constructor function
 *
 * @param {Object} config - config.lockit
 * @constructor
 */
const Adapter = module.exports = function (config) {

    if (!(this instanceof Adapter)) { return new Adapter(config); }

    this.config = config;

    // create connection string
    const uri = config.db.url + config.db.name;
    const sequelize = new Sequelize(uri, {
        storage: config.db.name
    });

    // tokens
    this.OauthToken = sequelize.define('OauthToken', {
        // make id like CouchDB and MongoDB
        _id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        // token
        access_token: Sequelize.STRING,
        access_token_expires_on: Sequelize.DATE,
        client_id: { type: Sequelize.STRING, unique: 'oauth_tokens_pkey' },
        refresh_token: Sequelize.STRING,
        refresh_token_expires_on: Sequelize.DATE,
        user_id: { type: Sequelize.INTEGER, unique: 'oauth_tokens_pkey' },
        // oauth code grant
        auth_code: Sequelize.STRING,
        auth_code_expires_on: Sequelize.DATE,
        // alexa link
        disabled: Sequelize.BOOLEAN
    }, {
        tableName: 'oauth_tokens',   // this will define the table's name
        timestamps: false                 // this will deactivate the timestamp columns
    });

    this.OauthToken.sequelize.sync({})
        .then(() => {
            // you can now use OauthToken to create new instances
            debug('OauthToken:sync: %s', 'OK');
            return null;
        })
        .error(err => {
            debug('OauthToken:throw: %s', JSON.stringify(err));
            throw err;
        });


    // oauth clients
    this.OauthClient = sequelize.define('OauthClient', {
        // make id like CouchDB and MongoDB
        _id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        // client
        client_id: { type: Sequelize.STRING, unique: 'oauth_clients_pkey' },
        client_secret: { type: Sequelize.STRING, unique: 'oauth_clients_pkey' },
        redirect_uri: Sequelize.STRING
    }, {
        tableName: 'oauth_clients',   // this will define the table's name
        timestamps: false                 // this will deactivate the timestamp columns
    });

    this.OauthClient.sequelize.sync({})
        .then(() => {
            // you can now use OauthClient to create new instances
            debug('OauthClient:sync: %s', 'OK');
            return null;
        })
        .error(err => {
            debug('OauthClient:throw: %s', JSON.stringify(err));
            throw err;
        });

};


/**
 * Create a new oauth client and save it to db.
 *
 * @param {String} clientId
 * @param {String} clientSecret
 * @param {String} redirectUri
 * @param {Function} done - Callback function having `err` and `client` as arguments
 */
Adapter.prototype.addClient = function (clientId, clientSecret, redirectUri, done) {
    var that = this;

    var query = {
        client_id: clientId,
        client_secret: clientSecret,
        redirect_uri: redirectUri
    };
    debug('addClient:query: %s', JSON.stringify(query));

    // save to db
    return that.OauthClient.upsert(query, {where: {client_secret: clientSecret, client_id: clientId}})
        .then(function(insType) {
            debug('addClient:type: %s', insType ? 'INSERT' : 'UPDATE');
            done(null);
        })
        .error(function(saveErr) {
            debug('addClient:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('addClient:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Find a oauth client data in db.
 *
 * @param {String} clientId
 * @param {String} clientSecret
 * @param {Function} done - Callback function having `err` and `client` as arguments
 */
Adapter.prototype.getClient = function (clientId, clientSecret, done) {
    var that = this;

    var query = {
        'client_id': clientId
    };
    if (clientSecret) { query.client_secret = clientSecret; }

    debug('getClient:query: %s', JSON.stringify(query));
    return that.OauthClient.find({ where: query })
        .then(function(client) {
            // create empty object in case no client is found
            var resClient = {};

            if (client) {
                resClient.clientId = client.dataValues.client_id;
                resClient.redirectUri = client.dataValues.redirect_uri;
            }

            debug('getClient:received: %s', JSON.stringify(client));
            done(null, resClient);
        })
        .error(function(err) {
            debug('getClient:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('getClient:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Is grant type allowed for this client
 *
 * @param {String} clientId
 * @param {String} grantType
 * @param {Function} done - Callback function having `err` and `allowed` as arguments
 */
Adapter.prototype.grantTypeAllowed = function (clientId, grantType, done) {
    // TODO: grant_type = 'authorization_code' always used
    var query = {
        clientId: clientId,
        grantType: grantType
    };
    debug('grantTypeAllowed:query: %s', JSON.stringify(query));
    debug('grantTypeAllowed:check: %s', JSON.stringify(grantType));
    done(null, (grantType === 'authorization_code' || grantType === 'refresh_token'));
};

/**
 * Save auth code
 *
 * @param {String} authCode
 * @param {String} clientId
 * @param {Date} expires
 * @param {String} user
 * @param {Function} done - Callback function having `err` as arguments
 */
Adapter.prototype.saveAuthCode = function (authCode, clientId, expires, user, done) {
    var that = this;

    var query = {
        auth_code: authCode,
        auth_code_expires_on: expires,
        user_id: user,
        client_id: clientId
    };
    debug('saveAuthCode:query: %s', JSON.stringify(query));
    debug('saveAuthCode:user: %s', JSON.stringify(user));

    // save to db
    return that.OauthToken.upsert(query, {where: {user_id: user, client_id: clientId}})
        .then(function(insType) {
            debug('saveAuthCode:type: %s', insType ? 'INSERT' : 'UPDATE');
            done(null);
        })
        .error(function(saveErr) {
            debug('saveAuthCode:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('saveAuthCode:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Get auth code params
 *
 * @param {String} authCode
 * @param {Function} done - Callback function having `err` and `{Object}authCode` as arguments
 */
Adapter.prototype.getAuthCode = function (authCode, done) {
    var that = this;

    debug('getAuthCode:query: %s', JSON.stringify(authCode));
    return that.OauthToken.find({where: {auth_code: authCode}})
        .then(function(auth) {
            // create empty object in case no auth is found
            var resAuth = {};

            if (auth) {
                resAuth.userId = auth.dataValues.user_id;
                resAuth.clientId = auth.dataValues.client_id;
                resAuth.expires = auth.dataValues.auth_code_expires_on;
            } else {
                debug('getAuthCode:notfound: %s', JSON.stringify(auth));
            }

            debug('getAuthCode:received: %s', JSON.stringify(auth));
            done(null, resAuth);
        })
        .error(function(err) {
            debug('getAuthCode:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('getAuthCode:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Save acc token
 *
 * @param {String} accessToken
 * @param {String} clientId
 * @param {Date} expires
 * @param {Object} user
 * @param {Function} done - Callback function having `err` as arguments
 */
Adapter.prototype.saveAccessToken = function (accessToken, clientId, expires, user, done) {
    var that = this;

    var query = {
        access_token: accessToken,
        access_token_expires_on: expires,
        client_id: clientId,
        user_id: user.id
    };
    debug('saveAccessToken:query: %s', JSON.stringify(query));
    debug('saveAccessToken:user: %s', JSON.stringify(user));

    // save to db
    return that.OauthToken.upsert(query, {where: {user_id: user.id, client_id: clientId}})
        .then(function(insType) {
            debug('saveAccessToken:type: %s', insType ? 'INSERT' : 'UPDATE');
            done(null);
        })
        .error(function(saveErr) {
            debug('saveAccessToken:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('saveAccessToken:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Get acc token params
 *
 * @param {String} bearerToken
 * @param {Function} done - Callback function having `err` and `{Object}accessToken` as arguments
 */
Adapter.prototype.getAccessToken = function (bearerToken, done) {
    var that = this;

    var query = {
        access_token: bearerToken
    };
    debug('getAccessToken:query: %s', JSON.stringify(query));
    return that.OauthToken.find({ where: query })
        .then(function(accessToken) {
            debug('getAccessToken:received: %s', JSON.stringify(accessToken));

            var respAccessToken = {};
            if (accessToken) {
                respAccessToken.expires = accessToken.dataValues.refresh_token_expires_on;
                respAccessToken.userId =  accessToken.dataValues.user_id;
                // or
                // respAccessToken.user =  {id: accessToken.dataValues.user_id};
            }
            debug('getRefreshToken:return: %s', JSON.stringify(respAccessToken));
            done(null, respAccessToken);
        })
        .error(function(err) {
            debug('getAccessToken:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('getAccessToken:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Get refresh token params
 *
 * @param {String} refreshToken
 * @param {Function} done - Callback function having `err` and `{Object}refreshToken` as arguments
 */
Adapter.prototype.getRefreshToken = function (refreshToken, done) {
    var that = this;

    var query = {
        refresh_token: refreshToken
    };
    debug('getRefreshToken:query: %s', JSON.stringify(query));
    return that.OauthToken.find({ where: query })
        .then(function(refreshToken) {
            debug('getRefreshToken:received: %s', JSON.stringify(refreshToken));

            var respRefreshToken = {};
            if (refreshToken) {
                respRefreshToken.clientId = refreshToken.dataValues.client_id;
                respRefreshToken.expires = refreshToken.dataValues.refresh_token_expires_on;
                respRefreshToken.userId = refreshToken.dataValues.user_id;
            }
            debug('getRefreshToken:return: %s', JSON.stringify(respRefreshToken));
            done(null, respRefreshToken);
        })
        .error(function(err) {
            debug('getRefreshToken:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('getRefreshToken:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Save refresh token
 *
 * @param {String} refreshToken
 * @param {String} clientId
 * @param {Date} expires
 * @param {Object} user
 * @param {Function} done - Callback function having `err` as arguments
 */
Adapter.prototype.saveRefreshToken = function (refreshToken, clientId, expires, user, done) {
    var that = this;

    var query = {
        refresh_token: refreshToken,
        refresh_token_expires_on: new Date(new Date().setFullYear(new Date().getFullYear() + 10)).toISOString(), // + 10 years //expires,
        client_id: clientId,
        user_id: user.id
    };
    debug('saveRefreshToken:query: %s', JSON.stringify(query));
    debug('saveRefreshToken:user: %s', JSON.stringify(user));

    // save to db
    return that.OauthToken.upsert(query, {where: {user_id: user.id, client_id: clientId}})
        .then(function(insType) {
            debug('saveRefreshToken:type: %s', insType ? 'INSERT' : 'UPDATE');
            done(null);
        })
        .error(function(saveErr) {
            debug('saveRefreshToken:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('saveRefreshToken:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};

/**
 * Check if any token exist
 *
 * @param {Number} user_id
 * @param done
 * @returns {*}
 */
Adapter.prototype.isUserLinked = function (user_id, done) {
    var that = this;

    var query = {
        user_id: user_id
    };
    debug('isUserLinked:user: %s', JSON.stringify(user_id));

    // drop all saved tokens
    return that.OauthToken.find({where: query})
        .then(function(token) {
            debug('isUserLinked: %s', JSON.stringify(token));
            done(null, token ? (token.disabled?1:2) : false);
        })
        .error(function(err) {
            debug('isUserLinked:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('isUserLinked:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};


/**
 * Drop all saved tokens
 *
 * @param {Number} user_id
 * @param done
 * @returns {*}
 */
Adapter.prototype.dropUserLinkage = function (user_id, done) {
    var that = this;

    var query = {
        user_id: user_id
    };
    debug('dropUserLinkage:user: %s', JSON.stringify(user_id));

    // drop all saved tokens
    return that.OauthToken.destroy({where: query})
        .then(function() {
            debug('dropUserLinkage: dropped');
            done(null, true);
        })
        .error(function(err) {
            debug('dropUserLinkage:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('dropUserLinkage:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};


/**
 * Disable Alexa access
 *
 * @param {Number} user_id
 * @param done
 * @returns {*}
 */
Adapter.prototype.disableUserLinkage = function (user_id, done) {
    var that = this;

    var query = {
        user_id: user_id
    };
    debug('disableUserLinkage:user: %s', JSON.stringify(user_id));

    // drop all saved tokens
    return that.OauthToken.update({disabled:true}, {where: query})
        .then(function() {
            debug('disableUserLinkage: dropped');
            done(null, true);
        })
        .error(function(err) {
            debug('disableUserLinkage:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('disableUserLinkage:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};


/**
 * Enable Alexa access
 *
 * @param {Number} user_id
 * @param done
 * @returns {*}
 */
Adapter.prototype.enableUserLinkage = function (user_id, done) {
    var that = this;

    var query = {
        user_id: user_id
    };
    debug('enableUserLinkage:user: %s', JSON.stringify(user_id));

    // drop all saved tokens
    return that.OauthToken.update({disabled:false}, {where: query})
        .then(function() {
            debug('enableUserLinkage: dropped');
            done(null, true);
        })
        .error(function(err) {
            debug('enableUserLinkage:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('enableUserLinkage:catched: %s', JSON.stringify(err));
            //done(err);
            throw err;
        });
};


/**
 * Get user data associated with access token
 *
 * @param {String} access_token - token
 * @returns {Promise} - promise
 */
Adapter.prototype.findAccessToken = function (access_token) {
    const that = this;

    return new Promise((resolve, reject) => {
        that.getAccessToken(access_token, (err, data) => {
            if (err) {
                return reject(err);
            }

            resolve(data);
        })
        .catch(e => {
            reject(e);
        });
    })
    .catch(e => {
        debug('findAccessToken:catched: %s', JSON.stringify(e));
        throw e;
    });
};
