/* global Promise */

'use strict';


const url = require('url');
const querystring = require('querystring');
const cfg = require('../config');

var parsedApiUrl = url.parse(cfg.sso.host);
const http = require(parsedApiUrl.protocol==="https:"?'https':'http');

const debug = require('debug')('screenlogic-echo:api');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

/**
 * Build http.request specific options
 *
 * @param {String} name - request name
 * @param {Object} data - request data
 * @returns {Object} options - {hostname: 'www.google.com', port: 80, path: '/upload', method: 'POST', headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': 35}}
 */
function buildHttpRequestOptions (name, data) {
    debug(`${name}:data: %s`, JSON.stringify(data));

    var apihost =  cfg.sso.host;
    // build full url for request
    let fullUrl = `${apihost}/${name}`;

    // add params if available
    if (data.params) {
        fullUrl = `${fullUrl}?${querystring.stringify(data.params)}`;

    }
    debug(`${name}:URL built: %s`, fullUrl);

    // create options from url
    const opts = url.parse(fullUrl);

    debug(`${name}:OPTIONS built: %s`, JSON.stringify(opts));
    return opts;
}


/**
 * Make request to API, get response data and check for errors
 *
 * @param {String} name - request name
 * @param {Object} data - request data
 * @returns {Promise} - promise for callAPI
 */
function callAPI (name, data) {
    return new Promise((resolve, reject) => {
        const options = buildHttpRequestOptions(name, data);

        let postData = '';

        const req = http.request(options, res => {
            debug(`${name}:status: %s`, res.statusCode);

            res.setEncoding('utf8');

            // collect full body from chunks
            let rawBody = '';
            let chunkCnt = 0;

            res.on('data', chunk => {
                chunkCnt += 1;
                rawBody += chunk;
                debug(`${name}:got BODY chunk: %s`, chunkCnt);
            });

            res.on('end', () => {
                debug(`${name}:request:end: No more data in response.`);

                // NOTE: thwSetHeatSp return nothing on success
                if (name === 'thwSetCircuitState'||'thwSetHeatSp' && res.statusCode === 200 && !rawBody) {
                    debug(`${name}:return nothing: %s`, JSON.stringify(rawBody));
                    return resolve({});
                }

                // other queries must return data
                try {
                    const body = JSON.parse(rawBody);
                    debug(`${name}:BODY: %s`, JSON.stringify(body));

                    if (body && body.errorMessage) {
                        return reject(body.errorMessage);
                    }

                    if (res.statusCode !== 200) {
                        return reject(`Connection error code: ${res.statusCode}`);
                    }

                    resolve(body);
                } catch (e) {
                    reject(e);
                }
            });
        });

        req.on('error', e => {
            if(e.code !== "HPE_INVALID_CONSTANT") {
                debug(`${name}:API request:error: %s`, e.message);
                reject(e.message);
            }
        });

        // write data to request body
        req.write(postData);
        req.end();
    })
    .catch(err => {
        debug(`${name}:catched: %s`, err.message);
        console.error(new Error(err));

        throw err.message || err;
    });
}


/**
 * Login
 *
 * @param {String} token
 * @returns {Promise} - promise for callAPI
 * @example {"email":"iosdev@midatlanticconsulting.com","access_token":"uOikOiEC","expires":"2017-11-29 18:16:21","refresh_token":"40666c4"}
 */
function apiLogin (token) {
    let params = {token,"system":"SL" };
    return callAPI('api/login', {params});
}

/**
 * Refresh Token
 *
 * @param {String} token
 * @returns {Promise} - promise for callAPI
 * @example {"sessionId": 7}
 */
function apiRefreshToken (token) {
    let params = {token };
    return callAPI('api/refresh', {params});
}

module.exports = {
    apiLogin,
    apiRefreshToken,
};
