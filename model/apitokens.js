'use strict';

var debug = require('debug')('screenlogic-echo:apitokens-model');
var Sequelize = require('sequelize');
var cfg = require('../config');

/**
 * Adapter constructor function
 *
 * @constructor
 */
var Adapter = module.exports = function () {

    if (!(this instanceof Adapter)) {return new Adapter(); }

    this.config = cfg.lockit;

    // create connection string
    var uri = this.config.db.url + this.config.db.name;
    var sequelize = new Sequelize(uri, {
        storage: this.config.db.name
    });

    this.apitokens = sequelize.define('apitokens', {
        // make id like CouchDB and MongoDB
        token: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        uid: {
            type: Sequelize.INTEGER,
            validate: {
                notEmpty: true,
            }
        },
        dt: {
            type: Sequelize.INTEGER,
        },
        
    }, {
        tableName: 'apitokens',   // this will define the table's name
         timestamps: false                 // this will deactivate the timestamp columns
    });

    this.apitokens.sequelize.sync({}).then(() => {
        // you can now use apitokens to create new instances
        debug('apitokens:sync: %s', 'OK');
        return null;
    })
    .error(err => {
        throw err;
    });
};


/**
 * Add new token to db.
 *
 * @param {String} token - token
 * @param {Number} uid - user id
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.addToken = function(token, uid, done) {
    var that = this;

    var query = {
        token: token,
        uid: uid,
    };
    debug('addToken:query: %s', JSON.stringify(query));

    var tkn = that.apitokens.build(query);

    // save to db
    return tkn.save()
        .then(function(ret) {
            debug('addToken:type: %s', JSON.stringify(ret));
            done(null);
        })
        .error(function(saveErr) {
            debug('addToken:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('addToken:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'You can\'t add this note.';
            done(err);
            //throw (err);
        });
};


/**
 * Find  apitokens by token
 *
 * @param {String} token
 * @param {Function} done
 * @returns {*}
 */
Adapter.prototype.getToken = function(token, done) {
    var that = this;

    var query = {
        token: token
    };
    debug('getToken:query: %s', JSON.stringify(query));

    // get apitokens
    return that.apitokens.find({where: query})
        .then(function(apitoken) {
            debug('getToken:result: %s', JSON.stringify(apitoken));
            apitoken = apitoken || {};
            done(null, apitoken);
        })
        .error(function(saveErr) {
            debug('getToken:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('getToken:catched: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};


/**
 * Remove an existing user message from db.
 *
 * @param {Number} _id - message id in db
 * @param {Function} done - Callback function having `err` and `res` arguments
 */
Adapter.prototype.delMessage = function( _id, done) {
    var that = this;

    var query = {
        _id: _id
    };
    debug('delMessage:query: %s', JSON.stringify(query));

    // find
    return that.apitokens.find({ where: query })
        .then(function(message) {
            if (!message) {return done(new Error('Error: Cannot find this message')); }
            return message.destroy()
                .then(function() {
                    debug('delMessage:result: %s', 'deleted');
                    done(null, true);
                })
                .error(function(err) {
                    debug('delMessage:error: %s', JSON.stringify(err));
                    done(err);
                })
                .catch(function (err) {
                    debug('delMessage:catched destroy: %s', JSON.stringify(err));
                    //done(err);
                    throw (err);
                });
        })
        .error(function(err) {
            debug('delMessage:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('delMessage:catched find: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};
