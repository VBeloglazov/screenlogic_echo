'use strict';

var debug = require('debug')('screenlogic-echo:messages-model');
var Sequelize = require('sequelize');
var cfg = require('../config');

/**
 * Adapter constructor function
 *
 * @constructor
 */
var Adapter = module.exports = function () {

    if (!(this instanceof Adapter)) {return new Adapter(); }

    this.config = cfg.lockit;

    // create connection string
    var uri = this.config.db.url + this.config.db.name;
    var sequelize = new Sequelize(uri, {
        storage: this.config.db.name
    });

    this.messages = sequelize.define('messages', {
        // make id like CouchDB and MongoDB
        _id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: true,
            }
        },
        body: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: true,
            }
        },
        dt: {
            type: Sequelize.INTEGER,
        },
        
    }, {
        tableName: 'messages',   // this will define the table's name
        timestamps: false                 // this will deactivate the timestamp columns
    });

    this.messages.sequelize.sync({}).then(() => {
        // you can now use messages to create new instances
        debug('Messages:sync: %s', 'OK');
        return null;
    })
    .error(err => {
        throw err;
    });
};


/**
 * Add new message to db.
 *
 * @param {String} title - message alias
 * @param {String} body - message id
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.addMessage = function(title, body, done) {
    var that = this;

    var query = {
        title: title,
        body: body,
    };
    debug('addMessage:query: %s', JSON.stringify(query));

    var message = that.messages.build(query);

    // save to db
    return message.save()
        .then(function(ret) {
            debug('addMessage:type: %s', JSON.stringify(ret));
            done(null);
        })
        .error(function(saveErr) {
            debug('addMessage:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('addMessage:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'You can\'t add this note.';
            done(err);
            //throw (err);
        });
};


/**
 * Edit note to db.
 *
 * @param {Number} id - note id in db
 * @param {String} title - note alias
 * @param {String} body - note alias
 * @param {Function} done - Callback function having `err` and `user` as arguments
 */
Adapter.prototype.updateMessage = function(id, title, body, done) {
    var that = this;

    var query = {
        title: title,
        body: body
    };
    debug('updateMessage:query: %s', JSON.stringify(query));

    // save to db
    return that.messages.update(query, {where: {_id: id}})
        .then(function(ret) {
            debug('updateMessage:type: %s', JSON.stringify(ret));
            done(null);
        })
        .error(function(saveErr) {
            debug('updateMessage:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('updateMessage:catched: %s', JSON.stringify(err));
            if (err.message === 'Validation error') err.message = 'Some values you set is wrong.';
            done(err);
            //throw (err);
        });
};


/**
 * Get all messages
 *
 * @param {Function} done
 * @returns {*}
 */
Adapter.prototype.getMessages = function () {

    // get messages
    return this.messages.findAll().then(messages => {
        // debug('getMssages:result: %s', JSON.stringify(messages));
        messages = messages || {};
        return messages;
    })
    .error(err => {
        debug('getMssages:error: %s', JSON.stringify(err));
        throw err;
    })
    .catch(e => {
        debug('getMssages:catched: %s', JSON.stringify(e));
        throw e;
    });
};


/**
 * Find  messages by id
 *
 * @param {Number} message_id
 * @param {Function} done
 * @returns {*}
 */
Adapter.prototype.getMessageById = function(message_id, done) {
    var that = this;

    var query = {
        _id: message_id
    };
    debug('getMessageById:query: %s', JSON.stringify(query));

    // get messages
    return that.messages.find({where: query})
        .then(function(message) {
            debug('getMessageById:result: %s', JSON.stringify(message));
            message = message || {};
            done(null, message);
        })
        .error(function(saveErr) {
            debug('getMessageById:error: %s', JSON.stringify(saveErr));
            done(saveErr);
        })
        .catch(function (err) {
            debug('getNoteById:catched: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};


/**
 * Remove an existing user message from db.
 *
 * @param {Number} _id - message id in db
 * @param {Function} done - Callback function having `err` and `res` arguments
 */
Adapter.prototype.delMessage = function( _id, done) {
    var that = this;

    var query = {
        _id: _id
    };
    debug('delMessage:query: %s', JSON.stringify(query));

    // find
    return that.messages.find({ where: query })
        .then(function(message) {
            if (!message) {return done(new Error('Error: Cannot find this message')); }
            return message.destroy()
                .then(function() {
                    debug('delMessage:result: %s', 'deleted');
                    done(null, true);
                })
                .error(function(err) {
                    debug('delMessage:error: %s', JSON.stringify(err));
                    done(err);
                })
                .catch(function (err) {
                    debug('delMessage:catched destroy: %s', JSON.stringify(err));
                    //done(err);
                    throw (err);
                });
        })
        .error(function(err) {
            debug('delMessage:error: %s', JSON.stringify(err));
            done(err);
        })
        .catch(function (err) {
            debug('delMessage:catched find: %s', JSON.stringify(err));
            //done(err);
            throw (err);
        });
};
