
/* global __dirname */

const alexaAppServer = require('alexa-app-server').start({
    port:8088,
    server_root : __dirname,
    public_html : 'public',
    app_dir:'apps',
    app_root : '/',

    preRequest : (json, request, response) => {
        const ret = json;

        ret.session.localStatus = {};

        // force router error
        // ret.session.localError = {type: 'USER', error: 'ACCOUNT_NOT_LINKED'};

        // fake local user
        ret.session.localStatus.localUserId = 2;

        // fake user devices
        ret.session.localStatus.localUserDev = [
            {'_id':4, 'user_id':2, 'alias':'Test', 'device_id':'Pentair: 00-00-00', 'password':'guest', 'enabled':true},
            {'_id':6, 'user_id':2, 'alias':'Home', 'device_id':'Pentair: E9-F4-DB', 'password':'', 'enabled':true},
         //   {'_id':5, 'user_id':2, 'alias':'Yard', 'device_id':'Pentair: EC-BF-16', 'password':'', 'enabled':false}
        ];


        // fake new request format
        ret.context = {
            'System': {
                'application': {
                    'applicationId': 'string'
                },
                'user': {
                    'userId': 'string',
                    'accessToken': 'string'
                },
                'device': {
                    'supportedInterfaces': {
                        'AudioPlayer': {}
                    }
                }
            },
            'AudioPlayer': {
                'token': 'string',
                'offsetInMilliseconds': 0,
                'playerActivity': 'string'
            }
        };

        return ret;
    }
});

// fake sl API
alexaAppServer.express.use(require('./routes/fake_sl_api.js'));
