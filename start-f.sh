#!/bin/sh

npm install && npm update

DEBUG='screenlogic-echo:*' DEBUG_FD=3 node ./bin/www 3>> log/$(date +"%Y-%m-%d")-debug.log 2>> log/$(date +"%Y-%m-%d")-error.log 1>> log/$(date +"%Y-%m-%d")-access.log 

