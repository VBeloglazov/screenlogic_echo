module.exports = {
  apps : [{
      name   : "slecho",
      script : "./bin/www",
      error_file : "log/err.log",
      out_file : "log/out.log",
      watch: true,
      ignore_watch : ["log", "node_modules"],
      env: {
        "SLCONF": "test",
        "NODE_ENV": "development",
      }
  }]
}
