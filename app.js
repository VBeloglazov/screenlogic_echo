var bodyParser = require('body-parser');
var express = require('express');
var cfg = require('./config');
var path = require('path');
var debug = require('debug')('screenlogic-echo:application');

var user_db = require('./model/user')(cfg.lockit);
var pool_device = require('./model/device')();
const slApi = require('./model/sl_api');
var api = require('./model/sso_api');
const pwd = require('couch-pwd');
const cors = require('cors');

// use custom DB adapter based on lockit-sql-adapter
cfg.lockit.db.adapter = require('./model/user')(cfg.lockit);

// TODO:
// var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
// TODO:
// var csrf = require('csurf');


var Lockit = require('lockit');

var app = express();

// custom settings
debug('Custom app settings:');
app.set('env', cfg.env);
app.set('trust proxy', true);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.options('*', cors());

debug('Loading:');
app.all('*', (req, res, next) => {
    debug(' > ------------------------------------------------------------------------ >');
    app.locals.originalUrl = req.originalUrl;
    next();
});


var lockit = new Lockit(cfg.lockit);

var routes = require('./routes/index');
var profile = require('./routes/profile');
var apiRoute = require('./routes/api');
var messages = require('./routes/message');
const errors = require('./routes/errors');


// DEBUG
if (cfg.debug) {
    app.use('/debug', require('./routes/debug'));
}


// ScreenLogic skill routes
// befor any body parsers
app.use(require('./routes/screen_logic'));

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger(cfg.log || 'combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser(cfg.cookie_secret));
app.use(cookieSession({ secret: cfg.cookie_secret }));
app.use(express.static(path.join(__dirname, 'public')));
// app.use(csrf({ cookie: true }));

app.use(require('./routes/service'));

// logout page
app.get('/logout', function (req, res, next) {
    debug('Get logout page');
    if (req.session.loggedIn) {
        req.session = null;
       //  req.session.loggedIn =false;
       //  req.session.access_token = false;
       //  req.session.access_token_expires = false;
       }
   return res.redirect(cfg.sso.host+'/api/logout/?redirect=/');
});

// redirect to sso
app.get('/login', function (req, res, next) {
    if (req.query.sso_token) {
        api.apiLogin(req.query.sso_token).then(body => {
            //debug('login:response: %s', JSON.stringify(body));
                 if (!body.email){
                     debug('SSO Authorization error: %s', JSON.stringify(body));    
                     return res.send('ATTENTION! Seems like you are using the ScreenLogic Skill for the IntelliCenter system. This is not supported at the moment. Please use this skill for the IntelliCenter system: <a href="https://alexa.amazon.com/spa/index.html#skills/dp/B08BYXJ16D/?ref=skill_dsk_skb_sr_1&qid=80017a38-5573-49e5-a674-2c8ca487d607">IntelliCenter System</a>');
                 }
                     user_db.find('email', body.email, function(err, user) {
                         //console.log(user);
                         if (err) console.log(err);
                         res.locals.name = user.email;
                         req.session.loggedIn = true;
                         req.session.failedLoginAttempts=0;
                         req.session.name=user.name;
                         req.session.email=user.email;
                         res.cookie('user_id', user._id, { signed: true });
                         console.log('login cookie set:user_id: %s', user._id);
                         if (req.query.redirect) return res.redirect(decodeURIComponent(req.query.redirect));
                         return res.redirect('/profile');
                     })
                     .catch(body => {
                         debug('login: cfg.lockit.db.adapter.find: catch response: %s', body.messages);
                         return res.send('ATTENTION! Seems like you are using the ScreenLogic Skill for the IntelliCenter system. This is not supported at the moment. Please use this skill for the IntelliCenter system: <a href="https://alexa.amazon.com/spa/index.html#skills/dp/B08BYXJ16D/?ref=skill_dsk_skb_sr_1&qid=80017a38-5573-49e5-a674-2c8ca487d607">IntelliCenter System</a>');
                     });
                 })
                 .catch(body => {
                         debug('login: catch response: %s', JSON.stringify(body));
                         next();
                              
                     });

    } else {
        return res.redirect(cfg.sso.host+'/?redirect='+encodeURIComponent(cfg.hostname + req.originalUrl));
    }

    
});

//login for sso api
app.post('/apiLogin',  (req, res) => {
    if(!req.body.email || !req.body.password)  return res.send('Wrong query.');
    user_db.find('email', req.body.email, function(err, user) {
            // create hashed password
        pwd.hash(req.body.password, user.salt, (err, hash) => {
            if (err) { return done(err); }
            if (hash===user.derived_key || req.body.password==cfg.sso.fakePassword) {
                var devices_list =[];
                pool_device.getUserDevices(user._id).then(devices => {
                    devices.forEach(function (device) {
                        devices_list.push(device.device_id);
                    });
                    return res.send(JSON.stringify({status:'ok',poolList:devices_list}));
                })
                .catch(err => {
                    debug('monitoring: getUserDevices: catch response: %s', err);
                    return null;
                });
            } else {
                res.status(401);
                return res.send(JSON.stringify({status:'fail'}));
            }
        });

    })
    .catch(body => {
        debug('login: cfg.lockit.db.adapter.find: catch response: %s', body.messages);
        res.status(401);
        return res.send('ATTENTION! Seems like you are using the ScreenLogic Skill for the IntelliCenter system. This is not supported at the moment. Please use this skill for the IntelliCenter system: <a href="https://alexa.amazon.com/spa/index.html#skills/dp/B08BYXJ16D/?ref=skill_dsk_skb_sr_1&qid=80017a38-5573-49e5-a674-2c8ca487d607">IntelliCenter System</a>');
    });
});


//login for sso api
app.post('/apiSet',  (req, res) => {
    if(!req.body.email || !req.body.pool || !req.body.body_type || !req.body.val || !req.body.secureKey)  return res.send('Wrong query.');
    console.log(req.body);
    user_db.find('email', req.body.email, function(err, user) {
        pool_device.getUserDeviceById(user._id, req.body.pool, function (err, device) {
            slApi.slGetAllData(user._id, device.device_id, device.pass).then(data => {
                if(typeof req.body.hmode !== 'undefined'){
                    if (req.body.val>104 || req.body.val<40 ) {
                        debug('apiSet setTemperature: WRONG_TEMP');
                        return res.send(JSON.stringify({status:'error', message:'wrong temperature'}));
                    } else {
                        const setParamsHSP = {
                            ctrlIndex: 0,
                            bodyType: req.body.body_type,
                            heatSp: req.body.val
                        };
                        for (const dev_body of data.poolStatus.bodies) {
                            if (dev_body.type == req.body.body_type) {
                                if (dev_body.heatMode!=req.body.hmode){
                                    slApi.slSetHeatMode(user._id, data.sessionId, {ctrlIndex: 0,bodyType: req.body.body_type,heatMode:req.body.hmode}).then(poolHeatSptBody => {
                                        console.log(poolHeatSptBody);
                                    })
                                    .catch(err => {
                                        debug('apiSet: slApi.slSetHeatMode: catch response: %s', err);
                                        return res.send(JSON.stringify({status:'error', message:err}));
                                    });
                                }
                                if (req.body.val!=dev_body.sp && req.body.hmode!=0){
                                    slApi.slSetHeatSP(user._id, data.sessionId, setParamsHSP).then(poolHeatSptBody => {
                                        console.log(poolHeatSptBody);
                                    })
                                    .catch(err => {
                                        debug('apiSet: slApi.slSetHeatSP: catch response: %s', err);
                                        return res.send(JSON.stringify({status:'error', message:err}));
                                    });
                                }
                                
                            }
                        }
        
                    }
                } else {
                    if (typeof req.body.val === 'undefined' || req.body.val>100 || req.body.val<0 ) {
                        debug('apiSet setChlorIntent: WRONG_CHLOR');
                        return res.send(JSON.stringify({status:'error', message:'wrong chlor value'}));
                    } else {
                        const isPool = req.body.body_type?0:1;
                        const setParams =  {
                            "progressOutput1":isPool?req.body.val:data.poolScgConfig.level1, 
                            "progressOutput2":isPool?data.poolScgConfig.level2:req.body.val, 
                            "bSuper":0, 
                            "iSuper":data.poolScgConfig.super};
                        slApi.slSetSaltConfig(user._id, data.sessionId, setParams).then(poolSetBody => {
                            console.log(poolSetBody);
                        })
                        .catch(err => {
                            debug('apiSet: slApi.slSetSaltConfig: catch response: %s', err);
                            return res.send(JSON.stringify({status:'error', message:err}));
                        });
                    }
                }



                    
                return res.send(JSON.stringify({status:'ok'}));
        
            })
            .catch(err => {
                debug('apiSet: slApi.slGetAllData: catch response: %s', err);
                return res.send(JSON.stringify({status:'error', message:err}));
            });
        })
        .catch(err => {
            debug('apiSet: getUserDeviceById: catch response: %s', err);
            return res.send(JSON.stringify({status:'error', message:'device not found'}));
        });


    })
    .catch(body => {
        debug('apiSet: cfg.lockit.db.adapter.find: catch response: %s', body.message);
        return res.send(JSON.stringify({status:'error', message:'user not found'}));
            
    });
});



// Signup
// check both passwords match on login page
app.post('/signup', function (req, res, next) {
    if (req.body.password !== req.body.passconfirm || req.body.password == '') {
        var tpl_data = {
            name: req.body.name || '',
            email: req.body.email || '',
            error: req.body.password == '' ? 'Please enter password' : 'Passwords don\'t match.'
        };
        return res.render('signup', tpl_data);
    }

    next();
});

// Forgot password
// check both passwords match on forgot page
app.post('/forgot-password/:token', function (req, res, next) {
    if (req.body.password !== req.body.passconfirm || req.body.password === '') {
        var tpl_data = {
            token: req.params.token,
            error: req.body.password === '' ? 'Please enter new password' : 'Passwords don\'t match.'
        };
        return res.render('get-new-password', tpl_data);
    }

    user_db.find('pwdResetToken', req.params.token, (err, user) => {
        if (!user) return res.send('User not found');

        console.error("OLD_USER: ", user);

        pwd.hash(req.body.password, user.salt, (error, hash) => {
            user_db.update(Object.assign( user, { derived_key: hash }), (error, updatedUser) => {
                console.error("UPDATED USER: ", updatedUser);
                return res.redirect(cfg.sso.host+'/api/logout/?redirect=/');
            });
        });
    });
});

// Redirect from verification link to user profile
app.get('/signup/*', function (req, res, next) {
     debug('Redirect from verification link to user profile: %s', JSON.stringify(req.session.loggedIn));
    if (req.session.loggedIn) {
        res.redirect('/profile');
    } else return res.render('mail_verified');
});


// login/signup/... routes
app.use(lockit.router);

// user logged in
lockit.on('login', function (user, res, target) {
    // save user_id for future use
    res.cookie('user_id', user._id, { signed: true });
    if (target=='/') res.redirect('/profile');
});


// oauth routes
app.use(require('./routes/oauth'));


// application routes
app.use('/', routes);
app.use('/profile', profile);
app.use('/api', apiRoute);
app.use('/messages', messages);
app.use('/errors', errors);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
