#!/bin/sh

npm install && npm update

# Save prev logs
mv log/debug.log log/debug.prev.log
mv log/error.log log/error.prev.log
mv log/access.log log/access.prev.log

DEBUG='screenlogic-echo:*' DEBUG_FD=3 node ./bin/www 3> log/debug.log 2> log/error.log 1> log/access.log

