'use strict';

const alexa = require('alexa-app');
const debug = require('debug')('screenlogic-echo:alexa-app');
const cfg = require('../../config');
const helpers = require('./helpers');
const strings = require('./strings');
const intents = require('./intents');
// TODO: var verifier = require('alexa-verifier');


/**
 * Define an alexa-app
 * @type {alexa.app}
 */
const app = new alexa.app('test');


/**
 * Customized Default Messages
 */
app.messages = strings.appMessages;


// Custom dictionary
app.dictionary = strings.appDictionary;


/**
 * Executed before any event handlers.
 * This is useful to setup new sessions, validate the applicationId, or do any other kind of validations.
 * Note: The post() method still gets called, even if the pre() function calls send() or fail().
 * The post method can always override anything done before it.
 *
 * @param {Object} request - alexa request
 * @param {Object} response - alexa responce
 * @param {String} type - request type
 * @returns {Boolean} - false for manual request send, true - auto
 */
app.pre = (request, response, type) => {
    debug(' >>> ---------------------------------------------------------------- >>>');
    debug(`pre:request type: ${type}`);

    // Note: all validations must be sync, otherwise intent will be resolved before validation complete
    // Note: exceptions will be catched by app.error, so not checked for `undefined`
    const applicationId = request.sessionDetails.application.applicationId;
    let isAppIdOk = false;
    for (const appId of cfg.applicationId) {
        if (applicationId == appId) {
            isAppIdOk = true;
            break;
        }
    }

    // bad appId
    if (!isAppIdOk) {
        request.data.session.localError = {
            type: 'REQUEST',
            error: 'BAD_APPLICATION_ID'
        };
        throw new Error(`applicationId not allowed: ${applicationId}`);
    }

    const localStatus = request.data.session.localStatus || null;
    if (localStatus.error) { throw new Error(localStatus.error); }

    // some errors can be explained to user
    // provide corresponding instructions
    if (request.data.session.localError) {
        // tell user something went wrong
        /*
        helpers.userNotReady(response, request.data.session.localError);
        return false; // stop with intent routing, all done manually
        */
        throw new Error('User profile not ready for requests');
    }

    debug('pre:validation: OK');
    if (type === 'IntentRequest' && !request.data.request.intent.name.match('AMAZON')) {
        return helpers.validateIntent(request, response);
    }
};


/*
 * respond for "talk to %AppName%" request
 */
app.launch(intents.launchIntent);


/*
 * respond for temperature status requests
 */
app.intent('poolTemperatureIntent',
    {
        'slots': {'POOLTYPE': 'LIST_OF_TYPES', 'DEVALIAS': 'LIST_OF_ALIAS'},
        'utterances': [
            'what {the|my|} {-|POOLTYPE} temperature {is|}',
            'what\'s {my|} {-|POOLTYPE} temperature',
            'what {the|my|} {-|DEVALIAS} {-|POOLTYPE} temperature {is|}',
            'what\'s {my|} {-|DEVALIAS} {-|POOLTYPE} temperature',
            'what temperature on {the|my|} {-|DEVALIAS} {-|POOLTYPE}',
            'what\'s temperature on {the|my|} {-|DEVALIAS} {-|POOLTYPE}'
        ]
    },
    intents.poolTemperatureIntent
);


/*
 * respond for set temperature status requests
 */
app.intent('poolSetTemperatureIntent',
{
    'slots': {'POOLTYPE': 'LIST_OF_TYPES', 'DEVALIAS': 'LIST_OF_ALIAS'},
    'utterances': [
        'what {the|my|} {-|POOLTYPE} set temperature {is|}',
        'what\'s {my|} {-|POOLTYPE} set temperature',
        'what {the|my|} {-|DEVALIAS} {-|POOLTYPE} set temperature {is|}',
        'what\'s {my|} {-|DEVALIAS} {-|POOLTYPE} set temperature',
        'what set temperature on {the|my|} {-|DEVALIAS} {-|POOLTYPE}',
        'what\'s set temperature on {the|my|} {-|DEVALIAS} {-|POOLTYPE}'
    ]
},
intents.poolSetTemperatureIntent
);

/*
 * respond for chemistry status requests
 */
app.intent('poolChemistryIntent',
    {
        'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CHEMTYPE': 'LIST_OF_CHEMS'},
        'utterances': [
            'what {the|my Pool|} {-|CHEMTYPE} {level|reading|} {is|}',
            'what\'s {the|my Pool|} {-|CHEMTYPE} {level|reading|}',
            'what {-|CHEMTYPE} {level|reading|} on {my|the|} {-|DEVALIAS} Pool',
            'what\'s {-|CHEMTYPE} {level|reading|} on {my|the|} {-|DEVALIAS} Pool'
        ]
    },
    intents.poolChemistryIntent
);


/*
 * respond for overall status requests
 */
app.intent('poolStatusIntent',
    {
        'slots': {'DEVALIAS': 'LIST_OF_ALIAS'},
        'utterances': [
            'what {the|my Pool|} status {is|}',
            'what\'s {the|my Pool|} status',
            'what {my|the|} {-|DEVALIAS} Pool status {is|}',
            'what\'s {my|the|} {-|DEVALIAS} Pool status'
        ]
    },
    intents.poolStatusIntent
);


/*
 * respond for overall status requests
 */
app.intent('poolCircuitsIntent',
{
    'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'POOLTYPE': 'LIST_OF_TYPES'},
    'utterances': [
        'what {the|my|} {-|POOLTYPE} circuits are on',
        'what {my|the|} {-|DEVALIAS} {-|POOLTYPE} circuits are on',
    ]
},
intents.poolCircuitsIntent
);

/*
 * respond for "set my Pool’s chlorine output to 51 percent" requests
 */
app.intent('setChlorIntent',
{
    'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'NUMBER':"AMAZON.NUMBER", 'POOLTYPE': 'LIST_OF_TYPES'},
    'utterances': [
        'set {the|my|} {-|POOLTYPE} chlorine output to {-|NUMBER} percent',
        'set {the|my|} {-|DEVALIAS} {-|POOLTYPE}  chlorine output to {-|NUMBER} percent'
    ]
},
intents.setChlorIntent
);


/*
 * respond for "turn on" requests
 */
app.intent('turnOnIntent',
    {
        'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CIRCUIT': 'LIST_OF_CIRCUITS', 'NUMBER':"AMAZON.NUMBER"},
        'utterances': [
            'turn on {the|my|} {-|CIRCUIT}',
            'turn on {the|my|} {-|DEVALIAS} Pool {-|CIRCUIT}',
            'turn on {the|my|} {-|CIRCUIT} and heat it to {-|NUMBER} degrees',
            'turn on {the|my|} {-|DEVALIAS} {-|CIRCUIT} and heat it to {-|NUMBER} degrees',
        ]
    },
    intents.turnOnIntent
);


/*
 * respond for "turn off" requests
 */
app.intent('turnOffIntent',
    {
        'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CIRCUIT': 'LIST_OF_CIRCUITS'},
        'utterances': [
            'turn off {the|my|} {-|CIRCUIT}',
            'turn off {the|my|} {-|DEVALIAS} Pool {-|CIRCUIT}'
        ]
    },
    intents.turnOffIntent
);

/*
 * respond for "set my Pool temperature to 87 degrees. " requests
 */
app.intent('setTemperatureIntent',
{
    'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CIRCUIT': 'LIST_OF_CIRCUITS','NUMBER':"AMAZON.NUMBER"},
    'utterances': [
        'set {the|my|} {-|CIRCUIT} temperature to {-|NUMBER} degrees',
        'set {the|my|} {-|DEVALIAS} {-|CIRCUIT} temperature to {-|NUMBER} degrees'
    ]
},
intents.setTemperatureIntent
);

/*
 * respond for "set my Pool’s heat mode to Heater " requests
 */
app.intent('setHeatModeIntent',
{
    'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CIRCUIT': 'LIST_OF_CIRCUITS', 'HEAT_MODE': 'LIST_OF_HEATMODE'},
    'utterances': [
        'set {the|my|} {-|CIRCUIT} heat mode to {-|HEAT_MODE}',
        'set {the|my|} {-|DEVALIAS} {-|CIRCUIT} heat mode to {-|HEAT_MODE}'
    ]
},
intents.setHeatModeIntent
);

/*
 * respond for pool (or Spa) is ready for swimming
 */
app.intent('poolReadySwimming',
{
    'slots': {'DEVALIAS': 'LIST_OF_ALIAS', 'CIRCUIT': 'LIST_OF_CIRCUITS'},
    'utterances': [
        '{if|} {the|my|} {-|DEVALIAS} {-|CIRCUIT} {is|} ready for swimming',
    ]
},
intents.poolReadySwimming
);

/*
 * Provide help about how to use the skill.
*/
app.intent('AMAZON.HelpIntent',
    {
        'utterances': [
            'what I can ask you'
        ]
    },
    intents.helpIntent
);

app.intent('AMAZON.FallbackIntent', intents.fallback);


/*
 * Let the user cancel a transaction or task (but remain in the skill)
 * Let the user completely exit the skill
*/
// TODO: temporarily disabled builtin intents as incomplete
app.intent('AMAZON.CancelIntent', {}, intents.cancelIntent);


/*
 * Let the user provide a positive response to a yes/no question for confirmation.
 */
// TODO: temporarily disabled builtin intents as incomplete
app.intent('AMAZON.YesIntent', {}, intents.yesIntent);


/*
 * Let the user provide a negative response to a yes/no question for confirmation.
 */
// TODO: temporarily disabled builtin intents as incomplete
app.intent('AMAZON.NoIntent', {}, intents.noIntent);


/*
 * Let the user provide a negative response to a yes/no question for confirmation.
 */
app.intent('AMAZON.StopIntent', {}, intents.stopIntent);


/*
 * SessionEndedRequest handler
 */
app.sessionEnded(intents.sessionEndedRequest);


/**
 * Error Handling
 * @param exception
 * @param request
 * @param response
 */
app.error = function (exception, request, response) {
    debug(`error catched: ${exception}`);
    debug(' <<< ---------------------------------------------------------------- <<<');

    if (request.data.session.localError) {
        debug('sending response with error description:');
        const error = request.data.session.localError;
        const errorID = error.type
            ? (error.error ? `${error.type}.${error.error}` : `${error.type}.DEFAULT`)
            : 'DEFAULT';

        debug('%s', errorID);
        return strings.error.createResponse(response, errorID);
    }

    // unhandled error
    debug('unhandled ERROR, router must catch this');
    // throw exception;
    // response.shouldEndSession(true).say(strings.error.getErrorDefault()).send();
    strings.error.logError(exception);
    return strings.error.createResponse(response, 'DEFAULT');
};


/**
 *  The last thing executed for every request.
 * @param request
 * @param response
 * @param type
 * @param exception
 */

app.post = function (request, response, type, exception) {
    debug('final: intent type: %s', JSON.stringify(type));

    // clean local data
    // request.data.session.localStatus = null;

    // TODO: store current intent for futher actions
    /*
    if (!response.response.shouldEndSession && !exception) {
        // TODO: check is request.isSessionNew === true when intent requested directly (w/o lanch)
        if (type === 'IntentRequest') {
            const intent = request.data.request.intent || {};
            debug('final: prevIntent to store: %s', JSON.stringify(intent));
            response.session('prevIntent', intent);
        }
    }
    */

    debug(' <<< ---------------------------------------------------------------- <<<');
    if (exception) {
        debug('catched by final, throwed to router');

        response.shouldEndSession(true).say(strings.error.getErrorDefault()).send();
        strings.error.logError(exception);
        return false;
    }

    // NOTE: clear() may brake response text in current version

    // Always turn an exception into a successful response
    // response.clear().say("An error occured: "+exception).send();
};


module.exports = app;
