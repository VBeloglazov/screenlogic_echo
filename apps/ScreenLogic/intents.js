/* global Promise */

'use strict';

const slApi = require('../../model/sl_api');
const debug = require('debug')('screenlogic-echo:alexa-app-intents');
const strings = require('./strings');
const helpers = require('./helpers');


/**
 * LaunchIntent, user "talk to AppName" request
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.launchIntent = function (request, response) {
    console.error('intent:launchIntent');

    // ask user for actions
    // response.shouldEndSession(false).say(strings.talk.LAUNCH_INTENT);
    return strings.createResponse(response, 'LAUNCH_INTENT');
};


/**
 * Deal with temperature intent
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
function poolTemperatureIntent (request, response) {
    const p = request.data.session.localValidated;

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        helpers.tellTemperatureStatus(request, response, data);
    })
    .catch(err => {
        helpers.sayApiConnectionError(request, response, err);
    });

    return false; // wait for async response
}
exports.poolTemperatureIntent = poolTemperatureIntent;
exports.poolSetTemperatureIntent = poolTemperatureIntent;

/**
 * Deal with chemistry intent
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.poolChemistryIntent = (request, response) => {
    const p = request.data.session.localValidated;

    console.error('poolChemistryIntent:access: %s', JSON.stringify(p.useDevice.allowed));

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        helpers.tellChemistryStatus(request, response, data);
    })
    .catch(err => {
        console.error('poolChemistryIntent:api:error: %s', JSON.stringify(err));
        helpers.sayApiConnectionError(request, response, err);
    });

    return false; // wait for async response
};


/**
 * Deal with set chlorine intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.setChlorIntent = (request, response) => {
    const p = request.data.session.localValidated;

    console.error('setChlorIntent:access: %s', JSON.stringify(p.useDevice.allowed));

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        //console.log(data);
        if (typeof p.statusReq.number === 'undefined' || p.statusReq.number>100 || p.statusReq.number<0 ) {
            debug('setChlorIntent: WRONG_CHLOR');
            return strings.createResponse(response, 'SET_INTENT.WRONG_CHLOR');
        } else {
            const usePoolType = p.statusReq.poolType ? p.statusReq.poolType : 'pool';
            const isPool = usePoolType.toLowerCase()==='pool'?1:0;
            const setParams =  {
                "progressOutput1":isPool?p.statusReq.number:data.poolScgConfig.level1, 
                "progressOutput2":isPool?data.poolScgConfig.level2:p.statusReq.number, 
                "bSuper":0, 
                "iSuper":data.poolScgConfig.super};
            return slApi.slSetSaltConfig(p.localStatus.localUserId, data.sessionId, setParams).then(poolSetBody => {
                debug('setChlorIntent:poolSetBody: %s', JSON.stringify(poolSetBody));
                strings.createResponse(response, 'ONOFF_INTENT.SET');
            });
        }
        //helpers.tellChemistryStatus(request, response, data);
    })
    .catch(err => {
        console.error('setChlorIntent:api:error: %s', JSON.stringify(err));
        helpers.sayApiConnectionError(request, response, err);
    });

    return false; // wait for async response
};


/**
 * Deal with status intent
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
function poolStatusIntent (request, response) {
    const p = request.data.session.localValidated;

    console.error('poolStatusIntent:access: %s', JSON.stringify(p.useDevice.allowed));

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        helpers.tellPoolStatus(request, response, data);
    })
    .catch(err => {
        console.error('poolStatusIntent:api:error: %s', JSON.stringify(err));
        helpers.sayApiConnectionError(request, response, err);
    });

    return false; // wait for async response
}
exports.poolStatusIntent = poolStatusIntent;
exports.poolCircuitsIntent = poolStatusIntent;
exports.poolReadySwimming = poolStatusIntent;

/**
 * Deal with circuit on/off intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
function turnOnOffIntent (request, response) {
    const p = request.data.session.localValidated;

    console.error('turnOnOffIntent:access: %s', JSON.stringify(p.useDevice.allowed));
    console.error('turnOnOffIntent:TEMP: %s', p.statusReq.number);

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        // use "pool" circuit as default
        const useCircuit = p.statusReq.circuitName ? p.statusReq.circuitName : 'pool';
        const curCircuit = helpers.findCircuitByName(useCircuit, data);
        console.error('turnOnOffIntent:circuit: %s', JSON.stringify(curCircuit));

        // circuit found
        if (typeof curCircuit.idStart !== 'undefined') {
            const onOff = request.data.request.intent.name === 'turnOnIntent' ? 1 : 0;
            const onoff = onOff === 1 ? 'on' : 'off';
            const setParams = {
                ctrlIndex: 0,
                circuitId: curCircuit.idStart,
                isOn: onOff
            };

            //remove "Pool" from Circuit;
            var CircuitWithoutPool = useCircuit.replace(new RegExp('pool', "ig"),'').trim();

            // set temperature
            if (typeof p.statusReq.number !== 'undefined' && !(p.statusReq.number*1)) {
                console.error("Provided temperature is not a number");
                return strings.createResponse(response, 'SET_INTENT.WRONG_TEMP');
            }

            if (typeof p.statusReq.number !== 'undefined') {
                
                if (p.statusReq.number>104 || p.statusReq.number<40 ) {
                    console.error('turnOnOffIntent: WRONG_TEMP');
                    return strings.createResponse(response, 'SET_INTENT.WRONG_TEMP');
                } else {
                    const body_type = curCircuit.name.toLowerCase()==='pool'?0:1;
                    const setParamsHSP = {
                        ctrlIndex: 0,
                        bodyType: body_type,
                        heatSp: p.statusReq.number
                    };
                    for (const dev_body of data.poolStatus.bodies) {
                        if (dev_body.type === body_type) {
                            if (dev_body.heatMode!==3){
                                slApi.slSetHeatMode(p.localStatus.localUserId, data.sessionId, {ctrlIndex: 0,bodyType: body_type,heatMode:3}).then(poolHeatSptBody => {
                                    console.error(poolHeatSptBody);
                                });
                            }
                            if (p.statusReq.number!==dev_body.sp){
                                slApi.slSetHeatSP(p.localStatus.localUserId, data.sessionId, setParamsHSP).then(poolHeatSptBody => {
                                    console.error(poolHeatSptBody);
                                });
                            }
                            break;
                        }
                    }

                }
                
            }
            // check getPoolsStatus for current status (on/off)
            // if (onoff === helpers.getCircuitCurrentStatus(curCircuit, data)) {
            //     console.error('turnOnOffIntent:already %s, nothing to do', JSON.stringify(onoff));
            //     // BUG: doesnt send response to alexa
            //     strings.createResponse(response, 'ONOFF_INTENT.ALREADY_SET_ON_OR_OFF', p.useDevice.alias, CircuitWithoutPool, onoff, onoff);
            // } else {
                // set circuit on/off
            return slApi.slSetCircuitOnOff(p.localStatus.localUserId, data.sessionId, setParams).then(poolSetBody => {
                console.error('turnOnOffIntent:onoffStatus: %s', JSON.stringify(poolSetBody));
                strings.createResponse(response, 'ONOFF_INTENT.SET');
            });
            // }
        }

        // circuit not available
        else {
            console.error('turnOnOffIntent:NOT FOUND: %s', JSON.stringify(p.statusReq.circuitName));
            // TODO: inform user if device have other circuits or have no any
            // const say = strings.formatTalk(strings.talk.ONOFF_INTENT.CIRCUIT_NOT_AVAILABLE, p.useDevice.alias, p.statusReq.circuitName);
            // response.say(say).send();
            strings.createResponse(response, 'ONOFF_INTENT.CIRCUIT_NOT_AVAILABLE', p.useDevice.alias, p.statusReq.circuitName);
        }
    })
    .catch(err => {
        console.error('turnOnOffIntent:api:error: %s', JSON.stringify(err.message));
        helpers.sayApiConnectionError(request, response, err);
    });

    return false; // wait for async response
}

exports.turnOnIntent = turnOnOffIntent;
exports.turnOffIntent = turnOnOffIntent;

/**
 * Deal with set temperature intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.setTemperatureIntent = (request, response)  =>  {
    const p = request.data.session.localValidated;

    console.error('setTemperatureIntent:access: %s', JSON.stringify(p.useDevice.allowed));
    console.error('setTemperatureIntent:TEMP: %s', p.statusReq.number);

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        // use "pool" circuit as default
        const useCircuit = p.statusReq.circuitName ? p.statusReq.circuitName : 'pool';
        if (typeof p.statusReq.number !== 'undefined') {
            if (typeof p.statusReq.number !== 'undefined' && !(p.statusReq.number*1)) {
                console.error("Provided temperature is not a number");
                return strings.createResponse(response, 'SET_INTENT.WRONG_TEMP');
            }
            
            if (p.statusReq.number>104 || p.statusReq.number<40 ) {
                debug('setTemperatureIntent: WRONG_TEMP');
                return strings.createResponse(response, 'SET_INTENT.WRONG_TEMP');
            } else {
                const body_type = useCircuit.toLowerCase()==='pool'?0:1;
                const setParamsHSP = {
                    ctrlIndex: 0,
                    bodyType: body_type,
                    heatSp: p.statusReq.number
                };
                for (const dev_body of data.poolStatus.bodies) {
                    if (dev_body.type === body_type) {
                        if (dev_body.heatMode!==3){
                            slApi.slSetHeatMode(p.localStatus.localUserId, data.sessionId, {ctrlIndex: 0,bodyType: body_type,heatMode:3}).then(poolHeatSptBody => {
                                console.log(poolHeatSptBody);
                            });
                        }
                        if (p.statusReq.number!==dev_body.sp){
                            slApi.slSetHeatSP(p.localStatus.localUserId, data.sessionId, setParamsHSP).then(poolHeatSptBody => {
                                console.log(poolHeatSptBody);
                            });
                        }
                        
                    }
                }

            }
            
        }
        else {
            debug('setTemperatureIntent: WRONG_TEMP');
            return strings.createResponse(response, 'SET_INTENT.WRONG_TEMP');
        }
        strings.createResponse(response, 'ONOFF_INTENT.SET');

    })
        .catch(err => {
            console.error('poolStatusIntent:api:error: %s', JSON.stringify(err));
            helpers.sayApiConnectionError(request, response, err);
        });

   
    return false; // wait for async response
};

/**
 * Deal with set heat mode intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.setHeatModeIntent = (request, response)  =>  {
    const p = request.data.session.localValidated;

    console.error('setHeatModeIntent:access: %s', JSON.stringify(p.useDevice.allowed));

    slApi.slGetAllData(p.localStatus.localUserId, p.useDevice.login, p.useDevice.pass).then(data => {
        // use "pool" circuit as default
        const useCircuit = p.statusReq.circuitName ? p.statusReq.circuitName : 'pool';
        const body_type = useCircuit.toLowerCase()==='pool'?0:1;
            
        if (typeof p.statusReq.heatmode === 'undefined' || typeof strings.HEATMODE[p.statusReq.heatmode.toLowerCase()]  === 'undefined' ) {
            debug('setHeatModeIntent: WRONG_HEATMODE '+p.statusReq.heatmode);
            return strings.createResponse(response, 'SET_INTENT.WRONG_HEATMODE');
        } else {
            const body_type = useCircuit.toLowerCase()==='pool'?0:1;
            for (const dev_body of data.poolStatus.bodies) {
                if (dev_body.type === body_type) {
                    if (dev_body.heatMode!==strings.HEATMODE[p.statusReq.heatmode.toLowerCase()]){
                        slApi.slSetHeatMode(p.localStatus.localUserId, data.sessionId, {ctrlIndex: 0,bodyType: body_type,heatMode:strings.HEATMODE[p.statusReq.heatmode.toLowerCase()]}).then(poolHeatSptBody => {
                            console.log(poolHeatSptBody);
                            
                        });
                        return strings.createResponse(response, 'SET_INTENT.SET');
                    } else {
                        debug('setHeatModeIntent:already %s, nothing to do', JSON.stringify(p.statusReq.heatmode));
                        return  strings.createResponse(response, 'SET_INTENT.SET');
                    }
                }
            }

        }

    }).catch(err => {
        console.error('poolStatusIntent:api:error: %s', JSON.stringify(err));
        helpers.sayApiConnectionError(request, response, err);
    });

   
    return false; // wait for async response
};


/**
 * Deal with help intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.helpIntent = function (request, response) {
    console.error('intent:helpIntent');

    // TODO: add more information
    // TODO: send card w/ full help version
    // TODO: use available devices data to provide more help
    // response.shouldEndSession(false).card(strings.cards.HELP).say(strings.talk.HELP);
    return strings.createResponse(response, 'HELP');
};


/**
 * TODO: Deal with cancel intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.cancelIntent = function (request, response) {
    console.error('intent:cancelIntent');

    const sessionPrevIntent = request.session('prevIntent');

    // TODO:
    // user say "No" on "Did you want to ask me something else?" question
    // switch (sessionPrevIntent) {
    //     case 'launchIntent':
    //     case 'helpIntent':
    //     case 'cancelIntent':
    //     case 'noIntent':
    //     case 'yesIntent':
    //         response.shouldEndSession(true).say(strings.talk.SESSION_ENDED);
    //         break;
    //
    //     default :
    //         response.shouldEndSession(false)
    //                 .say('Canceling request. <p>Did you want to ask me something else?</p>');
    // }

    return response.shouldEndSession(true).say(strings.talk.SESSION_ENDED);
};


/**
 * TODO: Deal with yes intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.yesIntent = function (request, response) {
    console.error('intent:yesIntent');

    const sessionPrevIntent = request.session('prevIntent');

    // TODO:
    // user say "Yes" on "Did you want to ask me something else?" question
    // switch (sessionPrevIntent) {
    //     case 'launchIntent':
    //     case 'helpIntent':
    //     case 'cancelIntent':
    //     case 'noIntent':
    //     case 'yesIntent':
    //         response.shouldEndSession(false).say('<p>What would it be?</p>');
    //         break;
    //
    //     default :
    //         response.shouldEndSession(false);
    //
    //         // TODO: respond w/ real data
    //         if (request.session('onYes')) {
    //             response.say('Repeating previous request for '+ request.session('onYes'))
    //                     .say('<p>Did you want to ask me something else?</p>');
    //         }
    // }

    return response.shouldEndSession(true).say(strings.talk.DOESNT_SUPPORTED);
};


/**
 * TODO: Deal with no intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.noIntent = function (request, response) {
    console.error('intent:noIntent');

    const sessionPrevIntent = request.session('prevIntent');

    // TODO:
    // user say "No" on "Did you want to ask me something else?" question
    // switch (sessionPrevIntent) {
    //     case 'launchIntent':
    //     case 'helpIntent':
    //     case 'cancelIntent':
    //     case 'noIntent':
    //     case 'yesIntent':
    //         response.shouldEndSession(true).say(strings.talk.SESSION_ENDED);
    //         break;
    //
    //     default :
    //         response.shouldEndSession(false)
    //                 .say('Canceling request. <p>Did you want to ask me something else?</p>');
    // }

    return response.shouldEndSession(true).say(strings.talk.DOESNT_SUPPORTED);
};

/**
 * Deal with fallback intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.fallback = function(request, response) {
    return strings.createResponse(response, 'DOESNT_SUPPORTED');
};

/**
 * Deal with stop intents
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.stopIntent = function (request, response) {
    console.error('intent:stopIntent');

    // TODO: Let the user stop an action (but remain in the skill)
    // user say "Stop"
    // response.shouldEndSession(true).say(strings.talk.SESSION_ENDED);
    return strings.createResponse(response, 'SESSION_ENDED');
};


/**
 * Respond for SessionEndedRequest
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.sessionEndedRequest = function (request, response) {
    console.error('SessionEndedRequest reseived');

    // response.shouldEndSession(true).say(strings.talk.SESSION_ENDED);
    return strings.createResponse(response, 'SESSION_ENDED');
};
