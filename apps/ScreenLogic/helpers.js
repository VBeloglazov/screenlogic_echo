/* global Promise */

'use strict';

const debug = require('debug')('screenlogic-echo:alexa-app-helpers');
const strings = require('./strings');
// const errors = require('./errors');


/**
 * Verify if all data provided to resolve request
 *
 * NOTE: everithing inside this function must be sync
 *       otherwise intent will be resolved before validation complete
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @returns {Boolean} - is ready for responce
 */
exports.validateIntent = (request, response) => {
    debug('validateIntent:start');

    const localStatus = request.data.session.localStatus || null;
    const devices = localStatus.localUserDev || [];

    const statusReq = getStatusRequest(request, devices);
    const availableDevices = listAvailableDevices(devices);

    // find device credentials if more than one device added
    const useDevice = findDeviceByAlias(statusReq.devAlias, availableDevices.devices);

    // store data for future reuse
    request.data.session.localValidated = {localStatus, devices, useDevice, statusReq, availableDevices};


    // if many devices and no alias provided
    if (statusReq.askAlias && availableDevices.count > 1) {
        debug('validateIntent:ASK_FOR_DEVICE_ALIAS: %s', JSON.stringify(availableDevices.talk));

        const string = strings.formatTalk(strings.talk.ASK_FOR_DEVICE_ALIAS, availableDevices.talk);
        response.say(string).send();
        return false;
    }

    // wrong alias
    if (statusReq.badAlias) {
        // and many devices
        if (availableDevices.count > 1) {
            debug('validateIntent: UNKNOWN_DEVICE_ALIAS: %s', JSON.stringify(statusReq.devAlias));
            const string = strings.formatTalk(strings.talk.UNKNOWN_DEVICE_ALIAS, availableDevices.talk);
            response.reprompt(string).send();
            return false;
        }

        // and one device
        if (statusReq.devAlias && availableDevices.count === 1) {
            debug('validateIntent: UNKNOWN_DEVICE_ALIAS: %s : IGNORED for one device', JSON.stringify(statusReq.devAlias));
            /*
            const string = strings.formatTalk(strings.talk.UNKNOWN_DEVICE_ALIAS, availableDevices.talk);
            response.say(string).send();
            return false;
            */

            // trick
            // TODO: it's ok if ONE available device and wrong ALIAS
            // TODO: can lead to correct response for "what my god damn pool temperature" ;)
            return true;
        }
    }

    // TODO: review this block
    // get device status
    //if (availableDevices.count === 1 || statusReq.devAlias) {


    // device available for alexa requests
    const isCorrectAliasRequested = isSrtictAlias(statusReq.devAlias, devices);
    const isSame = isSrtictAlias(statusReq.devAlias, [{alias:useDevice.alias}]); // TODO: trick, replace this
    const use_alias_talk = isCorrectAliasRequested ? `${statusReq.devAlias} pool` : 'this pool';

    if (useDevice.allowed && (!isCorrectAliasRequested || isSame)) {
        return true;
    }

    // Alexa access disabled for this device
    // user have one device but disabled
    if (availableDevices.count === 0) {
        debug('validateIntent:access denied: %s', JSON.stringify(useDevice));

        const string = strings.formatTalk(strings.talk.DEVICE_ACCESS_RESTRICTED_NO_OTHER, use_alias_talk);
        response.say(string)
            .card(strings.cards.DEVICE_ACCESS_RESTRICTED_NO_OTHER)
            .send();
        return false;
    }

    // user have other devices enabled, reprompt for one of them
    else {
        // TODO: collect information and store in session
        // TODO: wait for yes/no or device alias
        // response.session('availableAliases', availableDevices.aliases);

        if (availableDevices.count === 1) {
            const string = strings.formatTalk(strings.talk.DEVICE_ACCESS_RESTRICTED_OTHER_AVAILABLE, use_alias_talk, availableDevices.talk);
            response.say(string).send();

            // response.session('onYes', availableDevices.aliases);
            return false;
        }

        // reprompt for other device
        const string = strings.formatTalk(strings.talk.DEVICE_ACCESS_RESTRICTED_OTHER_AVAILABLE_MANY, use_alias_talk, availableDevices.talk);
        response.say(string).send();
        return false;

        // TODO: check/store correct values in session

        // store current intent name for futher actions
        // response.session('prevIntent', 'statusIntent');
    }

    //}

    //return true;
};


/**
 * Collect received data for status intent
 *
 * @param {Object} request - Alexa request
 * @param {Array} devices - user devices
 * @returns {nm$_helpers.getStatusRequest.sReq} - status
 */
function getStatusRequest (request, devices) {
    debug('getStatusRequest:got devices: %s', JSON.stringify(devices));
    const sReq = {
        poolType: '',
        chemType: '',
        circuitName: '',
        devAlias: '',
        noType: false,
        noCircuit: false,
        noAlias: false,
        badPool: false,
        badChem: false,
        badCircuit: false,
        badAlias: false,
        askAlias: false
    };

    // alexa-app fais when requesting unexpected slot, so check manually
    const slots = [
        {n:'poolType', v:'POOLTYPE'},
        {n:'chemType', v:'CHEMTYPE'},
        {n:'circuitName', v:'CIRCUIT'},
        {n:'devAlias', v:'DEVALIAS'},
        {n:'number', v:'NUMBER'},
        {n:'bodyname', v:'BODYNAME'},
        {n:'heatmode', v:'HEAT_MODE'},
    ];

    for (const slot of slots) {
        try {
            // request.slot drops error on not set slot query
            // sReq[slot.n] = request.slot(slot.v) || '';
            const reqSlot = request.data.request.intent.slots[slot.v] || {};
            if (typeof reqSlot.value !== 'undefined') {
                sReq[slot.n] = reqSlot.value;
            }
        } catch (e) {
            debug('getStatusRequest:NOT SET: %s', JSON.stringify(slot));
            debug('getStatusRequest:err: %s', JSON.stringify(e.message));
        }
    }

    debug('getStatusRequest:start sReq: %s', JSON.stringify(sReq));
    
    // no Pool body or chemistry detected in request
    if (sReq.poolType === '' && sReq.chemType === '') {
        debug('getStatusRequest:empty Type');
        sReq.noType = true;
        if (sReq.poolType === '') { sReq.badPool = true; }
        if (sReq.chemType === '') { sReq.badChem = true; }
    }

    // pool body of unknown type
    else if (sReq.poolType !== '' && strings.POOLTYPES.indexOf(sReq.poolType.toLowerCase()) === -1) {
        debug('getStatusRequest:badPoolType: %s', JSON.stringify(sReq.poolType));
        sReq.badPool = true;
    }

    // unknown chemistry
    else if (sReq.chemType !== '' && strings.CHEMTYPES.indexOf(sReq.chemType.toLowerCase()) === -1) {
        debug('getStatusRequest:badChemType: %s', JSON.stringify(sReq.chemType));
        sReq.badChem = true;
    }


    // no Pool cirquit detected in request
    if (sReq.circuitName === '') {
        debug('getStatusRequest:empty Circuit');
        sReq.noCircuit = true;
        sReq.badCircuit = true;
    }

    else {
        const gotCircuit = sReq.circuitName.toLowerCase();

        // unknown circuit
        // if (typeof strings.CIRCUITS[gotCircuit] === 'undefined') {
        //     debug('getStatusRequest:badCircuit: %s', JSON.stringify(sReq.circuitName));
        //     sReq.badCircuit = true;
        // }
    }


    // no device alias detected in request
    if (sReq.devAlias === '') {
        debug('getStatusRequest:empty Alias');
        sReq.noAlias = true;
        sReq.badAlias = true;
    }

    // check provided alias
    else {
        debug('getStatusRequest: Alias: %s', sReq.devAlias);
        const d = findDeviceByAlias(sReq.devAlias, devices);
        if (!d.alias || d.alias.toLowerCase() !== sReq.devAlias.toLowerCase()) {
            sReq.badAlias = true;
        }
    }

    // user have more then one device but no device alias detected in request
    if (devices.length > 1 && sReq.devAlias === '') {
        debug('getStatusRequest:emptyAlias but many devices: %s', JSON.stringify(devices.length));
        sReq.askAlias = true;
    }

    debug('getStatusRequest:collected sReq: %s', JSON.stringify(sReq));
    return sReq;
}


/**
 * List of not disabled devices
 *
 * @param {Array} devices - user device list
 * @returns {nm$_helpers.listAvailableDevices.available} - list, count ant text representation
 */
function listAvailableDevices (devices) {
    debug('listAvailableDevices:devices: %s', JSON.stringify(devices));

    const available = {
        devices: [],
        aliases: [],
        count: 0,
        talk: ''
    };

    let last = '';
    for (const device of devices) {
        if (device.enabled) {
            available.devices.push(device);
            available.count += 1;
            available.aliases.push(device.alias);

            if (available.count === 1) {
                available.talk = device.alias;
            } else {
                if (last !== '') {
                    available.talk = available.talk + ', ' + last;
                }
                last = device.alias;
            }
        }
    }

    // add 'AND' before last
    if (last !== '' && available.count > 1) {
        available.talk = available.talk + ' and ' + last;
    }

    // add 'Pool(s)' to the end
    if (available.count === 1) { available.talk += ' pool'; }
    else if (available.count >= 1) { available.talk += ' pools'; }


    debug('listAvailableDevices:available: %s', JSON.stringify(available));
    return available;
}


/**
 * List of not disabled devices
 *
 * @param {Array} bodies
 * @returns {nm$_helpers.listAvailableBodyTypes.available}
 */
function listAvailableBodyTypes (bodies) {
    debug('listAvailableBodyTypes:bodies: %s', JSON.stringify(bodies));

    const available = {
        bodies: bodies || [],
        names: [],
        count: bodies.length || 0,
        talk: ''
    };

    // TODO: max is 2, Pool and Spa, so no additional checks for now
    for (const body of bodies) {
        try {
            const tmpName = strings.POOLTYPES[body.type];

            if (available.names.length >= 1) {
                available.talk = available.talk + ' and ' + tmpName;
            }

            // first body
            else {
                available.talk = tmpName;
            }

            available.names.push(tmpName);
        } catch (e) {
            debug('listAvailableBodyTypes:error: %s', JSON.stringify(e.message));
            debug('listAvailableBodyTypes:error body: %s', JSON.stringify(body));
            strings.error.logError(e);
        }
    }

    return available;
}


/**
 * Get requested device credentials and status
 *
 * @param {String} devAlias - alias to find
 * @param {Object} devices - user devices
 * @returns {nm$_helpers.findDeviceByAlias.helpersAnonym$4|nm$_helpers.findDeviceByAlias.device} - found device
 */
function findDeviceByAlias (devAlias, devices) {
    debug('findDeviceByAlias:Alias: %s', JSON.stringify(devAlias));
    let device = {};

    try {
        // only one device set
        // if (!devAlias && devices.length === 1) {
        if (devices.length === 1) {
            debug('findDeviceByAlias:found ONE: %s', JSON.stringify(devices[0]));
            return {
                login: devices[0].device_id,
                pass: devices[0].password,
                allowed: devices[0].enabled,
                alias: devices[0].alias
            };
        }

        const reqAlias = devAlias.toString().toLowerCase();
        for (const dev of devices) {
            const curAlias = dev.alias.toString().toLowerCase();

            if (curAlias === reqAlias) {
                debug('findDeviceByAlias:found: %s', JSON.stringify(dev));
                device = {
                    login: dev.device_id,
                    pass: dev.password,
                    allowed: dev.enabled,
                    alias: dev.alias
                };
            }
        }
    } catch (e) {
        debug('findDeviceByAlias:error: %s', JSON.stringify(e.message));
        strings.error.logError(e);
    }

    return device;
}


/**
 * is requested device exist
 *
 * @param {String} devAlias - alias to find
 * @param {Object} devices - user devices
 * @returns {Boolean} - is device found or not
 */
function isSrtictAlias (devAlias, devices) {
    debug('isSrtictAlias:Alias: %s', JSON.stringify(devAlias));

    if (devAlias && devices.length >= 1) {
        try {
            const a = devAlias.toString().toLowerCase();

            // only one device set
            if (devices.length === 1) {
                const b = devices[0].alias.toString().toLowerCase();
                if (a !== b) {
                    debug('isSrtictAlias:NO: %s !== %s', devAlias, devices[0].alias);
                    return false;
                } else {
                    debug('isSrtictAlias:YES: %s === %s', devAlias, devices[0].alias);
                    return true;
                }
            }

            // many devices set
            else {
                for (const dev of devices) {
                    const b = dev.alias.toString().toLowerCase();

                    if (a === b) {
                        debug('isSrtictAlias:YES: %s === %s', devAlias, dev.alias);
                        return true;
                    }
                }
            }
        } catch (e) {
            debug('isSrtictAlias:error: %s', JSON.stringify(e.message));
            strings.error.logError(e);
        }
    }

    return false;
}


/**
 * Get requested circuit
 *
 * @param {String} circuitName - name to find
 * @param {Object} pool_data - all available pool data
 * @returns {Object} - circuit data
 */
exports.findCircuitByName = (circuitName, pool_data) => {
    debug('findCircuitByName:Circuit: %s', JSON.stringify(circuitName));
    let circuit = {};

    try {
        const reqCircuit = circuitName.toString().toLowerCase();
        //const reqCircuitRealName = strings.CIRCUITS[reqCircuit];

        let curCircuit = '';

        for (const circ of pool_data.poolCtrlConfig.circuitConfigs) {
            curCircuit = circ.name.toString().toLowerCase();
            if (curCircuit === reqCircuit) { //reqCircuitRealName.toLowerCase()
                circuit = circ;
            }
        }
    } catch (e) {
        debug('findCircuitByName:error: %s', JSON.stringify(e.message));
        strings.error.logError(e);
    }

    return circuit;
};


/**
 * Get requested circuit current status (on/off)
 *
 * @param {Object} circuit - circuit data
 * @param {Object} pool_data - all available pool data
 * @returns {String} - "on" or "off"
 */
exports.getCircuitCurrentStatus = (circuit, pool_data) => {
    debug('getCircuitCurrentStatus:Circuit: %s', JSON.stringify(circuit.name));

    const status = ['off', 'on'];

    for (const circ of pool_data.poolStatus.circuits) {
        if (circ.idStart === circuit.idStart) {
            return status[circ.onOff];
        }
    }

    return 'off';
};


/**
 * Generate responce text for temperature requests
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @param {Object} pool_data - all available pool data
 * @returns {undefined}
 */
exports.tellTemperatureStatus = (request, response, pool_data) => {
    debug('tellTemperatureStatus: --------');
    const p = request.data.session.localValidated;

    // use 'pool' as default type
    const usePoolType = p.statusReq.poolType ? p.statusReq.poolType : 'pool';
    const bodyType = strings.BODYTYPES[usePoolType];

    const temperatures = pool_data.poolStatus.bodies;
    const availableBodyTypes = listAvailableBodyTypes(temperatures);

    // user requested wrong pool body type
    if (p.statusReq.badPool && p.statusReq.poolType) {
        debug('tellTemperatureStatus: bad body type: %s', JSON.stringify(temperatures));

        // but one or more available in this device
        // TODO: repromt: 'You can ask me about Pool or Spa. Which will it be?'
        if (availableBodyTypes.count > 0) {
            // const say = strings.formatTalk(strings.talk.TEMPERATURE_INTENT.WRONG_BODY_TYPE, availableBodyTypes.talk);
            // response.say(say).send();
            strings.createResponse(response, 'TEMPERATURE_INTENT.WRONG_BODY_TYPE', availableBodyTypes.talk);
        }

        // and no pool body types available in this device
        else {
            strings.createResponse(response, 'TEMPERATURE_INTENT.NO_BODY_TYPES');
            // response.say(strings.talk.TEMPERATURE_INTENT.NO_BODY_TYPES).send();
        }
    }

    // requested pool body type is ok
    else {
        let haveTemperature = null;
        for (const dev_body of temperatures) {
            if (dev_body.type === bodyType) {
                debug('tellTemperatureStatus: temperature: %s', JSON.stringify(dev_body.temp));
                haveTemperature = true;
                /*
                const say = strings.formatTalk(
                    strings.talk.TEMPERATURE_INTENT.TEMPERATURE,
                    p.useDevice.alias, usePoolType, dev_body.temp
                );
                response.say(say).send();
                */
                if(request.data.request.intent.name==="poolTemperatureIntent") {
                    strings.createResponse(response, 'TEMPERATURE_INTENT.TEMPERATURE', p.useDevice.alias, usePoolType, dev_body.temp);
                } else {
                    strings.createResponse(response, 'TEMPERATURE_INTENT.SET_TEMPERATURE', p.useDevice.alias, usePoolType, dev_body.sp);
                }
                break;
            }
        }

        // temperature unavailable
        if (!haveTemperature) {
            strings.createResponse(response, 'TEMPERATURE_INTENT.TEMPERATURE_UNAVAILABLE', p.useDevice.alias, usePoolType);
            // response.say(strings.talk.TEMPERATURE_INTENT.TEMPERATURE_UNAVAILABLE).send();
        }
    }
};


/**
 * Generate responce text for chemistry requests
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @param {Object} pool_data - all available pool data
 * @returns {undefined}
 */
exports.tellChemistryStatus = (request, response, pool_data) => {
    debug('tellChemistryStatus: --------');

    const p = request.data.session.localValidated;
    const ph = pool_data.chemData.ph; 
    const orp = pool_data.chemData.orp;
    const intelliStatus = getIntelliStatus(pool_data);
    const salt = intelliStatus.IntelliChlor? pool_data.poolScgConfig.salt:'N/A';
    // let say = '';


    // pH state requested
    if (p.statusReq.chemType.toLowerCase() === 'ph') {
        // say = strings.formatTalk(strings.talk.CHEMISTRY_INTENT.PH, p.useDevice.alias, ph);
        strings.createResponse(response, 'CHEMISTRY_INTENT.PH', p.useDevice.alias, ph);
    }

    // ORP state requested
    else if (p.statusReq.chemType.toLowerCase() === 'orp') {
        // say = strings.formatTalk(strings.talk.CHEMISTRY_INTENT.ORP, p.useDevice.alias, orp);
        strings.createResponse(response, 'CHEMISTRY_INTENT.ORP', p.useDevice.alias, orp);
    }

    // Salt state requested
    else if (p.statusReq.chemType.toLowerCase() === 'salt') {
        // say = strings.formatTalk(strings.talk.CHEMISTRY_INTENT.ORP, p.useDevice.alias, orp);
        strings.createResponse(response, 'CHEMISTRY_INTENT.SALT', p.useDevice.alias, salt);
    }

    // TODO: add something like "Not fully understand your request? But ..." if bad chemType
    // Chemistry state requested
    else {
        // say = strings.formatTalk(strings.talk.CHEMISTRY_INTENT.CHEMISTRY, p.useDevice.alias, ph, orp);
        strings.createResponse(response, 'CHEMISTRY_INTENT.CHEMISTRY', p.useDevice.alias, ph, orp);
    }

    // response.say('<p>Anithing else?</p>').shouldEndSession(false).send();
    // response.say(say).send();
};


/**
 * List running circuits
 *
 * @param {Object} pool_data - all available pool data
 * @returns {nm$_helpers.getRunningCircuits.running} - list, count ant text representation
 */
function getRunningCircuits (pool_data) {
    debug('getRunningCircuits:start');

    const running = {
        circuits: [],
        names: [],
        count: 0,
        talk: ''
    };

    let last = '';
    for (const circuit of pool_data.poolStatus.circuits) {
        if (circuit.onOff === 1) {
            running.circuits.push(circuit);
            running.count += 1;

            // get circuit name
            let circuit_name = 'unknown';
            for (const conf of pool_data.poolCtrlConfig.circuitConfigs) {
                if (conf.idStart === circuit.idStart) {
                    circuit_name = conf.name;
                    break;
                }
            }
            running.names.push(circuit_name);

            if (running.count === 1) {
                running.talk = circuit_name;
            } else {
                if (last !== '') {
                    running.talk = running.talk + ', ' + last;
                }
                last = circuit_name;
            }
        }
    }

    // add 'AND' before last
    if (last !== '' && running.count > 1) {
        running.talk = running.talk + ' and ' + last;
    }

    // add 'circuit(s)' to the end
    if (running.count === 1) { running.talk += ' circuit is'; }
    else if (running.count >= 1) { running.talk += ' circuits are'; }
    else if (running.count === 0) { running.talk = 'No circuits are'; }


    debug('getRunningCircuits:running: %s', JSON.stringify(running));
    return running;
}


// TODO: check this
function getIntelliStatus (pool_data) {
    const presentID = {
        IntelliChlor: 4, // 0x00000004
        IntelliChem: 32768 // 0x00008000
    };

    const status = {
        IntelliChem: false,
        IntelliChlor: false
    };
//debug('getIntelliStatus:pool_data: %s', JSON.stringify(pool_data));
    const equipFlags = pool_data.poolCtrlConfig.equipFlags;
    //debug('getIntelliStatus:equipFlags: %s', equipFlags);

    // get status
    if (equipFlags >= 0) {
        for (const c in status) {
            status[c] = ((parseInt(equipFlags)&presentID[c]) > 0 );
            // debug(`getIntelliStatus:${c}:  ${status[c]}`);
        }
    }

    return status;
}
exports.getIntelliStatus = getIntelliStatus;


/**
 * Generate responce text for pool status requests
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @param {Object} pool_data - all available pool data
 * @returns {undefined}
 */
exports.tellPoolStatus = (request, response, pool_data) => {
    debug('tellPoolStatus: --------');
    const p = request.data.session.localValidated;
    const intelliStatus = getIntelliStatus(pool_data);

    const bodyType = strings.BODYTYPES.pool;
    const temperatures = pool_data.poolStatus.bodies;

    // Pool temperature status
    let temperature = 'unavailable';
    for (const dev_body of temperatures) {
        if (dev_body.type === bodyType) {
            temperature = dev_body.temp;
            break;
        }
    }
    debug('tellPoolStatus: temperature: %s', JSON.stringify(temperature));

    //debug('tellPoolStatus:pool_data: %s', JSON.stringify(pool_data));

    // Pool chems
    const ph = pool_data.chemData.ph;  //pool_data.poolStatus.ph;
    const orp =  pool_data.chemData.orp; // pool_data.poolStatus.orp;
    const salt = intelliStatus.IntelliChlor? pool_data.poolScgConfig.salt:'N/A';
    const running = getRunningCircuits(pool_data);  
    
    debug('tellPoolStatus:ph: %s', ph);
    debug('tellPoolStatus:orp: %s', orp);
   // debug('tellPoolStatus:salt: %s, chemData[30]: %s, poolScgConfig.salt: ', salt, chemData[30], pool_data.poolScgConfig.salt);

    let say = '';
    if(request.data.request.intent.name==="poolCircuitsIntent") {
        say = strings.formatTalk(
            strings.talk.STATUS_INTENT.INTELLI_CIRCUITS,
        //    p.useDevice.alias,
            running.talk
        );
    } else if(request.data.request.intent.name==="poolReadySwimming") {
        if(temperature<80) {
            say = strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_HAVE_TEMP_LESS,
                p.useDevice.alias
            );
        }
        if(intelliStatus.IntelliChem && orp<650) {
            say += strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_HAVE_ORP_LESS,
                p.useDevice.alias
            );
        }
        if(intelliStatus.IntelliChem && orp>800) {
            say += strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_HAVE_ORP_OVER,
                p.useDevice.alias
            );
        }
        if(intelliStatus.IntelliChem && ph<7.2) {
            say += strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_HAVE_PH_LESS,
                p.useDevice.alias
            );
        }
        if(intelliStatus.IntelliChem && ph>8) {
            say += strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_HAVE_PH_OVER,
                p.useDevice.alias
            );
        }
        if(say === ''){
            say += strings.formatTalk(
                strings.talk.READY_SWIMMING_INTENT.INTELLI_OK,
                p.useDevice.alias
            );
        }
    } else if (intelliStatus.IntelliChem && intelliStatus.IntelliChlor) {
        say = strings.formatTalk(
            strings.talk.STATUS_INTENT.INTELLI_HAVE_CHEM_AND_CHLOR,
            p.useDevice.alias,
            temperature, ph, orp, salt,
            running.talk
        );
    } else if (intelliStatus.IntelliChem) {
        say = strings.formatTalk(
            strings.talk.STATUS_INTENT.INTELLI_HAVE_ONLY_CHEM,
            p.useDevice.alias,
            temperature, ph, orp,
            running.talk
        );
    } else if (intelliStatus.IntelliChlor) {
        say = strings.formatTalk(
            strings.talk.STATUS_INTENT.INTELLI_HAVE_ONLY_CHLOR,
            p.useDevice.alias,
            temperature, salt,
            running.talk
        );
    } else {
        say = strings.formatTalk(
            strings.talk.STATUS_INTENT.INTELLI_NO_CHEM_AND_CHLOR,
            p.useDevice.alias,
            temperature,
            running.talk
        );
    }

    response.say(say).send();
};


/**
 * Api request responded w/ error
 *
 * @param {Object} request - Alexa request
 * @param {Object} response - Alexa response
 * @param {String} error - error
 * @returns {undefined}
 */
exports.sayApiConnectionError = (request, response, error) => {
    console.error('sayApiConnectionError:api:error: %s', JSON.stringify(error));

    // TODO: extended api errors detection
    // api respond: {"errorMessage": "System offline"}
    if (error.error && error.error.includes('System offline')) {
        const p = request.data.session.localValidated;
        strings.error.createResponse(response, 'API.SYSTEM_OFFLINE', p.useDevice.alias);
    } else if (error.error && error.error.includes('System not registered')) {
        const p = request.data.session.localValidated;
        strings.error.createResponse(response, 'API.SYSTEM_NOT_REGISTERED', p.useDevice.alias);
    } else if (error.errorMessage && error.errorMessage.includes('rejected login')) {
        const p = request.data.session.localValidated;
        strings.error.createResponse(response, 'API.LOGIN_REJECTED', p.useDevice.alias);
    }
    // other api error
    else {
        strings.error.createResponse(response, 'API.DEFAULT');
    }
};
