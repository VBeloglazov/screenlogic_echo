'use strict';

const cfg = require('../../config');
const util = require('util');
exports.error = require('./errors');

// Customized Default Messages
exports.appMessages = {
    NO_INTENT_FOUND: 'I know nothing about that intent.'
};

// Custom dictionary
// TODO: not used due to custom slot types
exports.appDictionary = {
    'types': ['Pool', 'Spa'],
    'chems': ['pH', 'ORP', 'Chemistry'],
    'alias': ['Home', 'Garden', 'Yard'],
    'circits': ['Pool', 'Spa', 'Aux', 'Feature', 'AuxEx']
};

// SL device specific
exports.BODYTYPES = {'pool': 0, 'spa': 1};
exports.POOLTYPES = ['pool', 'spa'];
exports.CHEMTYPES = ['ph', 'orp', 'chemistry', 'salt'];
exports.HEATMODE  = {'off': 0, 'solar only': 1, 'solar preferred': 2, 'heater': 3, 'heat pump': 1, 'heat pump preferred': 2};

// allowed circuit names
// key: got by Alexa, value: real circuit name
exports.CIRCUITS = {
    'aerator':      'Aerator',
    'air blower':   'Air Blower',
    'aux 1':        'Aux 1',
    'aux 2':        'Aux 2',
    'aux 3':        'Aux 3',
    'aux 4':        'Aux 4',
    'aux 5':        'Aux 5',
    'aux 6':        'Aux 6',
    'aux 7':        'Aux 7',
    'aux 8':        'Aux 8',
    'aux 9':        'Aux 9',
    'aux 10':       'Aux 10',
    'aux extra':    'Aux Extra',
    'backwash':     'Backwash',
    'back light':   'Back Light',
    'bbq light':    'BBQ Light',
    'beach light':  'Beach Light',
    'booster pump': 'Booster Pump',
    'bug light':    'Bug Light',
    'cabana lts':   'Cabana Lts',
    'cabana lights':'Cabana Lts',       // alias, let user pronounce full name
    'cavefall':     'Cavefall',
    'chem feeder':  'Chem. Feeder',
    'chemical feeder':'Chem. Feeder',   // alias, let user pronounce full name
    'chemistry feeder':'Chem. Feeder',  // alias, let user pronounce full name
    'chlorinator':  'Chlorinator',
    'cleaner':      'Cleaner',
    'color wheel':  'Color Wheel',
    'deck light':   'Deck Light',
    'drain line':   'Drain Line',
    'drive light':  'Drive Light',
    'edge pump':    'Edge Pump',
    'entry light':  'Entry Light',
    'fan':          'Fan',
    'fiber optic':  'Fiber Optic',
    'fiber works':  'Fiber Works',
    'fill line':    'Fill Line',
    'floor clnr':   'Floor Clnr',
    'floor cleaner':'Floor Clnr',       // alias, let user pronounce full name
    'fogger':       'Fogger',
    'fountain':     'Fountain',
    'fountain 1':   'Fountain 1',
    'fountain 2':   'Fountain 2',
    'fountain 3':   'Fountain 3',
    'fountains':    'Fountains',
    'front light':  'Front Light',
    'garden lts':   'Garden Lts',
    'garden lights':'Garden Lts',       // alias, let user pronounce full name
    'gazebo lts':   'Gazebo Lts',
    'gazebo lights':'Gazebo Lts',       // alias, let user pronounce full name
    'high speed':   'High Speed',
    'hi-temp':      'Hi-Temp',
    'high temp':    'Hi-Temp',          // alias, let user pronounce full name
    'house light':  'House Light',
    'jets':         'Jets',
    'laminars':     'Laminars',
    'lights':       'Lights',
    'low speed':    'Low Speed',
    'lo-temp':      'Lo-Temp',
    'low temp':     'Lo-Temp',         // alias, let user pronounce full name
    'mist':         'Mist',
    'music':        'Music',
    'not used':     'Not Used',
    'ozonator':     'Ozonator',
    'path lights':  'Path Lights',
    'patio lights': 'Patio Lights',
    'pg2000':       'Pg2000',
    'pg 2000':      'Pg2000',           // alias, let user pronounce full name
    'pond light':   'Pond Light',
    'pool pump':    'Pool Pump',
    'pool':         'Pool',
    'pool high':    'Pool High',
    'pool light':   'Pool Light',
    'pool lights':  'Pool Lights',
    'pool low':     'Pool Low',
    'pool sam':     'Pool Sam',
    'pool sam 1':   'Pool Sam 1',
    'pool sam 2':   'Pool Sam 2',
    'pool sam 3':   'Pool Sam 3',
    'security lt':  'Security Lt',
    'security light':'Security Lt',     // alias, let user pronounce full name
    'slide':        'Slide',
    'solar':        'Solar',
    'spa':          'Spa',
    'spa high':     'Spa High',
    'spa light':    'Spa Light',
    'spa low':      'Spa Low',
    'spa sal':      'Spa Sal', // TODO: alias: salt? salter?
    'spa sam':      'Spa Sam', // TODO: ?
    'spa wtrfll':   'Spa Wtrfll',
    'spa waterfall':'Spa Wtrfll',       // alias, let user pronounce full name
    'spillway':     'Spillway',
    'sprinklers':   'Sprinklers',
    'stream':       'Stream',
    'statue lt':    'Statue Lt',
    'statue light': 'Statue Lt',        // alias, let user pronounce full name
    'swim jets':    'Swim Jets',
    'wtr feature':  'Wtr Feature',
    'water feature':'Wtr Feature',      // alias, let user pronounce full name
    'wtr feat lt':  'Wtr Feat Lt',
    'water feat light':'Wtr Feat Lt',   // alias, let user pronounce full name
    'waterfall':    'Waterfall',
    'waterfall 1':  'Waterfall 1',
    'waterfall 2':  'Waterfall 2',
    'waterfall 3':  'Waterfall 3',
    'whirlpool':    'Whirlpool',
    'wtrfl lght':   'Wtrfl Lght',
    'waterfall light':'Wtrfl Lght',     // alias, let user pronounce full name
    'yard light':   'Yard Light',
    'feature 1':    'Feature 1',
    'feature 2':    'Feature 2',
    'feature 3':    'Feature 3',
    'feature 4':    'Feature 4',
    'feature 5':    'Feature 5',
    'feature 6':    'Feature 6',
    'feature 7':    'Feature 7',
    'feature 8':    'Feature 8'
};


/**
 * Text to speak
 */
exports.talk = {
    LAUNCH_INTENT:
        `Pentair's ${cfg.appname} allows you to interact with your IntelliTouch and EasyTouch pool and spa control system.`
        + '<p>You can turn your pool, spa, and various circuits on or off, ask for temperature or chemistry readings, or say help</p>',

    SESSION_ENDED:
        'Good-bye.',

    DOESNT_SUPPORTED:
        'Sorry, but we do not support this phrase. With our  skill you can turn your pool, spa, and various circuits on or off, ask for temperature or chemistry readings, or say help',

    // "Your have Home and Yard pools."
    ASK_FOR_DEVICE_ALIAS:
        'You have %s.'
        + '<p>Please repeat your request for one of them</p>',

    UNKNOWN_DEVICE_ALIAS:
        'I know nothing about requested pool. But you can ask me about %s.'
        + '<p>Please repeat your request for one of them</p>',

    DEVICE_ACCESS_RESTRICTED_NO_OTHER:
        'Access to %s is restricted.'
        + `<p>Your have no other pools enabled in your ${cfg.appname} account.</p>`
        + `<p>Please use the ${cfg.appname} App to enable one or more pools.</p>`,

    DEVICE_ACCESS_RESTRICTED_OTHER_AVAILABLE:
        'Access to %s is restricted.'
        + '<p>But Your have %s enabled.</p>'
        + '<p>Please repeat your request for this one</p>',

    DEVICE_ACCESS_RESTRICTED_OTHER_AVAILABLE_MANY:
        'Access to %s is restricted.'
        + '<p>But Your have %s enabled.</p>'
        + '<p>Please repeat your request for one of them</p>',

    HELP:

        'Asking Pentair pool to tell me my pool status will tell you the <say-as interpret-as="characters">pH</say-as>,'
        +'<say-as interpret-as="characters">ORP</say-as>, Salt Level and Circuit status.'
        +'<p>If you ask it to turn on or off one of your existing pool system circuits, it will turn it on or off.</p>'
        +'<p>For example, you could say</p>'
        +'<p>Alexa, ask Pentair pool what my pool temperature is, or</p>'
        +'<p>Alexa, ask Pentair pool to turn on my Spa</p>'
        +'<p>You can also say, stop, if you’re done.</p>',

    TEMPERATURE_INTENT: {
        NO_BODY_TYPES:
            'Your poll have no body types available for requests.',
        WRONG_BODY_TYPE:
            'Your pool have no such body type. But you can ask me about %s.',
        TEMPERATURE_UNAVAILABLE:
            'Your %s %s temperature state is unavailable.',
        TEMPERATURE:
            'Your %s %s is currently <say-as interpret-as="number">%s</say-as> degrees.',
        SET_TEMPERATURE:
            'Your %s %s temperature set to <say-as interpret-as="number">%s</say-as> degrees.',
    },

    CHEMISTRY_INTENT: {
        PH:
            'Your %s pool <say-as interpret-as="characters">pH</say-as> level is <say-as interpret-as="number">%s</say-as>.',
        ORP:
            'Your %s pool <say-as interpret-as="characters">ORP</say-as> is <say-as interpret-as="number">%s</say-as>.',
        SALT:
            'Your %s pool salt level is <say-as interpret-as="number">%s</say-as> <say-as interpret-as="characters">ppm</say-as>.',
        CHEMISTRY:
            'Your %s pool <say-as interpret-as="characters">pH</say-as> reading is <say-as interpret-as="number">%s</say-as>'
            + ' and <say-as interpret-as="characters">ORP</say-as> is <say-as interpret-as="number">%s</say-as>.'
    },

    STATUS_INTENT: {
        INTELLI_HAVE_CHEM_AND_CHLOR:
            'Your %s pool is currently <say-as interpret-as="number">%s</say-as> degrees'
            + ', <say-as interpret-as="characters">pH</say-as> is <say-as interpret-as="number">%s</say-as>'
            + ' and <say-as interpret-as="characters">ORP</say-as> is <say-as interpret-as="number">%s</say-as>.'
            + ' Your salt level is <say-as interpret-as="number">%s</say-as> <say-as interpret-as="characters">ppm</say-as>.'
            + ' %s currently running.',
        INTELLI_HAVE_ONLY_CHEM:
            'Your %s pool is currently <say-as interpret-as="number">%s</say-as> degrees'
            + ', <say-as interpret-as="characters">pH</say-as> is <say-as interpret-as="number">%s</say-as>'
            + ' and <say-as interpret-as="characters">ORP</say-as> is <say-as interpret-as="number">%s</say-as>.'
            + ' %s currently running.',
        INTELLI_HAVE_ONLY_CHLOR:
            'Your %s pool is currently <say-as interpret-as="number">%s</say-as> degrees'
            + ' and your salt level is <say-as interpret-as="number">%s</say-as> <say-as interpret-as="characters">ppm</say-as>.'
            + ' %s currently running.',
        INTELLI_NO_CHEM_AND_CHLOR:
            'Your %s pool is currently <say-as interpret-as="number">%s</say-as> degrees.'
            + ' %s currently running.',
        INTELLI_CIRCUITS:
            'Your %s currently on.',
    },

    READY_SWIMMING_INTENT: {
        INTELLI_HAVE_TEMP_LESS:
            'Your %s pool is a little on the cold side currently, you might want to turn on the heater. ',
        INTELLI_HAVE_ORP_LESS:
            'Your %s pool is low on chlorine. I’d recommend adding more before you swim. ',
        INTELLI_HAVE_ORP_OVER:
            'Your %s pool has a high chlorine level. I’d recommend letting it lower a bit before swimming. ',
        INTELLI_HAVE_PH_LESS:
            'Your %s pool has a low pH. I’d recommend adding some soda ash to raise your pH so your eyes don’t get irritated while swimming. ',
        INTELLI_HAVE_PH_OVER:
            'Your %s pool has a high pH. I’d recommend adding some baking soda to lower your pH so your eyes don’t get irritated while swimming. ',
        INTELLI_OK:
            'Your %s pool is all ready for swimming.',
    },

    ONOFF_INTENT: {
        SET:
            'OK',
        ALREADY_SET_ON_OR_off:
            'You asked me to turn your %s pool %s %s, but this circuit already %s.',
        CIRCUIT_NOT_AVAILABLE:
            'Your %s pool have no circuit named %s'
    },
    SET_INTENT: {
        SET:
            'OK',
        WRONG_TEMP  :
            'Temperature must be in range <say-as interpret-as="number">40</say-as> to <say-as interpret-as="number">104</say-as> degrees',
        WRONG_CHLOR  :
            'Chlorine  must be in range <say-as interpret-as="number">0</say-as> to <say-as interpret-as="number">100</say-as> percent',
        WRONG_ORP  :
            '<say-as interpret-as="characters">ORP</say-as> must be in range <say-as interpret-as="number">400</say-as> to <say-as interpret-as="number">800</say-as>',
        WRONG_PH  :
            '<say-as interpret-as="characters">PH</say-as> must be in range <say-as interpret-as="number">7.2</say-as> to <say-as interpret-as="number">7.6</say-as>',
        ALREADY_SET_HEATMODE:
            'You asked me to set your %s %s heat mode to %s, but heat mode already  %s.',
        WRONG_HEATMODE :
            "Can't recognize the heat mode, please use heater, solar only, solar preferred or off as a heat mode.",
    }
};


/**
 * Keep session opened and ask for an action
 */
exports.reprompt = {
    LAUNCH_INTENT:
        'What would you like?',

    HELP:
        'Asking Pentair Pool to tell me my pool status will tell you the pH, ORP, Salt Level and Circuit status. If you ask it to turn on or off one of your existing pool system circuits, it will turn it on or off. For example, you could say Alexa, ask Pentair Pool what my pool temperature is, or Alexa, ask Pentair Pool to turn on my Spa. You can also say, stop, if you’re done.'
};


/**
 * Also show a card in Alexa App
 */
exports.cards = {
    HELP: {
        type: 'Simple',
        title: 'Help',
        content: 'Here are some things you can say:\n'
            + 'Alexa, ask Pentair pool to turn on my Spa\n'
            + 'Alexa, ask Pentair pool what the pool temperature is\n'
            + 'Alexa, ask Pentair pool what my pool status is'
    },

    DEVICE_ACCESS_RESTRICTED_NO_OTHER: {
        type: 'Simple',
        title: 'Access restricted',
        content: 'You disabled access to requested pool\n'
            + `Your have no other pools enabled in your ${cfg.appname} account.\n`
            + `You can enable or disable them in your ${cfg.hostname} profile by ckicking pool alias and setting corresponding option.`
    }
};


/**
 * Format string, replace all %s with variables
 * Note: If there are more arguments passed to the util.format() method than the number of placeholders,
 * the extra arguments are coerced into strings (for objects and symbols, util.inspect() is used)
 * then concatenated to the returned string, each delimited by a space.
 *
 * @param {String} string - input string
 * @param {type} vars - one or more variables
 * @returns {exports.format.str} - output string
 */
exports.formatTalk = (string, ...vars) => util.format(string, ...vars);


/**
 * Generate response
 *
 * @param {Object} response - Alexa response
 * @param {String} stringID - responce ID
 * @param {type} vars - one or more variables to add in 'talk' string
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.createResponse = (response, stringID, ...vars) => {
    const s = stringID.split('.');
    const str = {
        talk: s.length === 1 ? exports.talk[stringID] : exports.talk[s[0]] && exports.talk[s[0]][s[1]],
        reprompt: s.length === 1 ? exports.reprompt[stringID] : exports.reprompt[s[0]] && exports.reprompt[s[0]][s[1]],
        card: s.length === 1 ? exports.cards[stringID] : exports.cards[s[0]] && exports.cards[s[0]][s[1]]
    };

    // talk
    if (str.talk) {
        response.say(util.format(str.talk, ...vars));
    }

    // keep session and wait for request
    if (str.reprompt) {
        response.shouldEndSession(false).reprompt(str.reprompt);
    }

    // show card
    if (str.card) {
        response.card(str.card);
    }

    // send response
    response.send();

    return false; // wait for send()
};
