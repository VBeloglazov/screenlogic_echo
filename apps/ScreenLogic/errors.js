'use strict';

const cfg = require('../../config');
const util = require('util');

/*
 * TYPE: {ERROR_ID: 'text to say'}
 * ERROR_ID === null - use DEFAULT text
 */
exports.talk = {
    REQUEST: {
        BAD_SESSION: null, // DEFAULT used
        BAD_APPLICATION_ID: 'This skill is not supported anymore. Please use new version of the skill.', // DEFAULT used

        DEFAULT:
            `Your request rejected by ${cfg.appname}. Details are sent to Alexa App.`
    },

    USER: {
        ACCOUNT_NOT_LINKED:
            `Your ${cfg.appname} account is not linked. Please use the Alexa App to link the account.`,

        ACCOUNT_LINKAGE_DISABLED:
            `Your ${cfg.appname} account linkage disabled. You can enable it back in your profile.`,

        ACCOUNT_NO_DEVICES:
            `You have no pools added in your ${cfg.appname} account. Please check the Alexa App for instructions.`,

        ACCOUNT_DEVICES_DISABLED:
            `You have no pools enabled in your ${cfg.appname} account. Please check the Alexa App for instructions.`,

        DEFAULT:
            `Your ${cfg.appname} account not configured.`
    },

    API: {
        SYSTEM_OFFLINE:
            'The requested system is offline, please check internet connection to your ScreenLogic system and try again.',
        LOGIN_REJECTED:
            'Incorrect password. Please login to pentairpoolchat.com and update your password for ScreenLogic system.',
        SYSTEM_NOT_REGISTERED:
            'System is not registered, please check internet connection to your ScreenLogic system and try again.',
        DEFAULT:
            'Can\'t connect to requested pool. Please try again later.'
    },

    DEFAULT:
        'Sorry, something bad happened. Please try again later.'
};


/**
 * Keep session opened and ask for an action
 */
exports.reprompt = {};


/*
 * eTYPE: {ERROR_ID: {oCard}}
 */
exports.cards = {
    USER: {
        ACCOUNT_NOT_LINKED: {
            type: 'LinkAccount'
        },
        ACCOUNT_LINKAGE_DISABLED: {
            type: 'Simple',
            title: `Your ${cfg.appname} account linkage disabled.`,
            content: `You can enable it back in your ${cfg.hostname} profile using "Link my Alexa" option.`
        },
        ACCOUNT_NO_DEVICES:{
            type: 'Simple',
            title: `Your have no pools added in your ${cfg.appname} account.`,
            content: `You can add them in your ${cfg.hostname} profile using "Add device" option.`
        },
        ACCOUNT_DEVICES_DISABLED: {
            type: 'Simple',
            title: `Your have no pools enabled in your ${cfg.appname} account.`,
            content: `You can enable or disable them in your ${cfg.hostname} profile by ckicking pool alias and setting corresponding option.`
        }
    },

    REQUEST: {
        DEFAULT:{
            type: 'Simple',
            title: 'Bad request!',
            content: `Please use ${cfg.appname} to report a bug.\n${cfg.hostname}`
        }
    }
};


/*
 * Get error or card text
 */
function getError (error, text) {
    let say = false;

    if (exports[text][error.type]) {
        if (exports[text][error.type][error.error]) {
            // found exact error
            say = exports[text][error.type][error.error];
        } else if (exports[text][error.type].DEFAULT) {
            // say type default error text
            say = exports[text][error.type].DEFAULT;
        } else if (exports[text].DEFAULT) {
            // say default error text
            say = exports[text].DEFAULT;
        }
    } else if (exports[text].DEFAULT) {
        // say default error text
        say = exports[text].DEFAULT;
    }

    return say;
}

exports.getErrorText = error => getError(error, 'talk');
exports.getErrorCard = error => getError(error, 'cards');
exports.getErrorDefault = () => getError({type:'DEFAULT', error:null}, 'talk');
exports.logError = (...params) => console.error(Date(), ...params);


/**
 * Format string, replace all %s with variables
 * Note: If there are more arguments passed to the util.format() method than the number of placeholders,
 * the extra arguments are coerced into strings (for objects and symbols, util.inspect() is used)
 * then concatenated to the returned string, each delimited by a space.
 *
 * @param {String} string - input string
 * @param {type} vars - one or more variables
 * @returns {exports.format.str} - output string
 */
exports.formatTalk = (string, ...vars) => util.format(string, ...vars);


/**
 * Generate response
 *
 * @param {Object} response - Alexa response
 * @param {String} stringID - responce ID
 * @param {type} vars - one or more variables to add in 'talk' string
 * @returns {Boolean} false - wait for async and respond manually
 */
exports.createResponse = (response, stringID, ...vars) => {
    const s = stringID.split('.');
    const str = {
        talk: s.length === 1 ? exports.talk[stringID] : exports.talk[s[0]] && exports.talk[s[0]][s[1]],
        reprompt: s.length === 1 ? exports.reprompt[stringID] : exports.reprompt[s[0]] && exports.reprompt[s[0]][s[1]],
        card: s.length === 1 ? exports.cards[stringID] : exports.cards[s[0]] && exports.cards[s[0]][s[1]]
    };

    // talk
    if (str.talk) {
        response.say(util.format(str.talk, ...vars));
    }

    // keep session and wait for request
    if (str.reprompt) {
        response.shouldEndSession(false).reprompt(str.reprompt);
    }

    // show card
    if (str.card) {
        response.card(str.card);
    }

    // send response
    response.send();

    return false; // wait for send()
};
