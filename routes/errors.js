const express = require('express');
const router = express.Router();
const cfg = require('../config');
const errorsTable = require('../model/app_errors')(cfg.lockit);
const pool_device = require('../model/device')();
var cors = require('cors');

router.get('/', cors(), function(req, res, next) {
    const { search, d_from, d_to } = req.query;

    console.error("PARAMS: ", search, d_from, d_to);

    return errorsTable.findAll({ where: {
            date: {
                $gte: d_from,
                $lte: d_to
            },
            $or: {
                mail: {
                    $like: `%${search}%`
                },
                device_id: {
                    $like: `%${search}%`
                }
            }

        }
    })
        .then(response => {
            res.send(response);
        })
        .catch(error => console.error(error));
});

/*Get device info by id*/
router.get('/device/:id', cors(), function(req, res, next) {
    if (!req.params.id*1) {
        res.status(400);
        res.send(new Error('Bad id param'));
    }

    return pool_device.getUserDevices(req.params.id);
});

module.exports = router;