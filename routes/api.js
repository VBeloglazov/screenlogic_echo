var express = require('express');
var debug = require('debug')('screenlogic-echo:profile-router');
var cfg = require('../config');
var utils = require('lockit-utils');
var router = express.Router();
var pool_device = require('../model/device')();
var apitokens = require('../model/apitokens')();
var oauth2 = require('../model/oauth2')(cfg.lockit);
var slApi = require('../model/sl_api');
var user_db = require('../model/user')(cfg.lockit);
var ssoapi = require('../model/sso_api');

/* pre route */
router.all('/*', function(req, res, next) {
    if (req.headers.sso_token) {
        apitokens.getToken( req.headers.sso_token, function (err, apitoken) {
            if (err) {
                console.log(err.message);
            } else {
                if (apitoken.uid){
                    user_db.find('_id', apitoken.uid, function(err, user) {
                        res.locals.user = user;
                        next();
                    })
                    .catch(body => {
                        debug('login: cfg.lockit.db.adapter.find: catch response: %s', body.messages);
                        return res.send('Authorization error.');
                            
                    });
                } else {
                    ssoapi.apiLogin(req.headers.sso_token).then(body => {
                        //debug('login:response: %s', JSON.stringify(body));
                             if (!body.email){
                                 debug('SSO Authorization error: %s', JSON.stringify(body));    
                                 return res.send('Authorization error.');
                             }
                                 user_db.find('email', body.email, function(err, user) {
                                    res.locals.user = user;
                                     if (err) console.log(err);
                                     apitokens.addToken( req.headers.sso_token, user._id, function (err) {if (err) console.log(err);});
                                     next();
                                 })
                                 .catch(body => {
                                     debug('login: cfg.lockit.db.adapter.find: catch response: %s', body.messages);
                                     return res.send(JSON.stringify({status:'error','message':'Authorization error.'}));
                                         
                                 });
                             })
                             .catch(body => {
                                     debug('login: catch response: %s', JSON.stringify(body));
                                     return res.send(JSON.stringify({status:'error','message':'Authorization error.'}));
                                          
                                 });
                }

            }
        });
    } else {
        console.log('ne ok');
    }
    // return res.send('STOP');
    });
// router.all('/*', function(req, res, next) {
//     // check user linkage
//     if (res.locals.user._id) {
//         oauth2.isUserLinked(res.locals.user._id, function(err, isLinked) {
//             if (err) {
//                 debug('pre:isUserLinked:error: %s', JSON.stringify(err));
//                 res.locals.tpl_data.error = 'An error occured during linkage check!';
//                 res.locals.tpl_data.isLinked = false;
//             }
//             else {
//                 debug('pre:isUserLinked:status %s', JSON.stringify(isLinked));
//                 res.locals.tpl_data.isLinked = isLinked;
//                 res.locals.tpl_data.isLinkEnabled = (isLinked === 2);
//             }

//             next();
//         });
//     } else {
//         res.locals.tpl_data.isLinked = false;
//         next();
//     }
// });


/* profile page. */
router.get('/getDevices',  (req, res) => {
    pool_device.getUserDevices(res.locals.user._id).then(devices => {
        var devices_list =[];
        devices.forEach(function (device) {
            devices_list.push({id:device._id,device_id:device.device_id,password:device.password,alias:device.alias,enabled:device.enabled});
        });
        return res.send(JSON.stringify(devices_list));
    })
    .catch(err => {
        console.log(err);
        return res.send(JSON.stringify({}));
    });
});

/* profile page. */
router.post('/importDevices',  (req, res) => {
    pool_device.getUserDevices(res.locals.user._id).then(devices => {
        var devices_list =[], statuses = [];
        devices.forEach(function (device) {
            devices_list.push(device.device_id);
        });
        let newdevices = JSON.parse(req.body.devices);
        data2save = [];
        var re = /Pentair: [A-Fa-f0-9]{1,2}-[A-Fa-f0-9]{1,2}-[A-Fa-f0-9]{1,2}/;
        newdevices.forEach(function (device) {
            if (!re.test(device.device_id)) {
                statuses.push({device_id:device.device_id,alias:device.alias, status:0, message:"Wrong device ID"});
            } else if (devices_list.includes(device.device_id)) {
                statuses.push({device_id:device.device_id,alias:device.alias, status:0, message:"Duplicate"});
            } else {
                statuses.push({device_id:device.device_id,alias:device.alias, status:1, message:""});
                devices_list.push(device.device_id);
                data2save.push(device);
            }
            
        });
        pool_device.importDevices(res.locals.user._id,data2save).then(devices => {
            console.log(devices.length);
        })
        .catch(err => {
            console.log(err);
            return res.send(JSON.stringify({}));
        });

        return res.send(JSON.stringify(statuses));
    })
    .catch(err => {
        console.log(err);
        return res.send(JSON.stringify({}));
    });
});



// check device via API
router.post('/addDevice',  function(req, res, next) {
    req.body.device_id = "Pentair: "+req.body.device_id;
    if(req.body.alias.trim().toLowerCase().endsWith('pool')) req.body.alias = req.body.alias.trim().slice(0,-4).trim();
    if(req.body.alias.trim().toLowerCase().startsWith('pool')) req.body.alias = req.body.alias.trim().slice(4).trim();
    debug('add:device: %s', JSON.stringify(req.body));

    const params = [res.locals.user._id, req.body.device_id, req.body.password];

    slApi.slLogin(...params).then(body => {
        next();
    })
    .catch(err => {
        debug('add:error %s', JSON.stringify(err));
        return res.send(JSON.stringify({status:'error','message':'Device ID or password is wrong!'}));
    });
});

router.post('/addDevice',  function(req, res) {
    debug('add:user_id %s', JSON.stringify(res.locals.user._id));

    pool_device.addDevice(res.locals.user._id, req.body.alias, req.body.device_id, req.body.password, function (err) {
        if (err) {
            debug('add:errors %s', JSON.stringify(err.message));
            return res.send(JSON.stringify({status:'error','message':err.message}));

        } else {
            return res.send(JSON.stringify({status:'successful'}));
        }
    });
});



// check device via API
router.post('/editDevice',  function(req, res, next) {
    debug('edit:device: %s', JSON.stringify(req.body));
    req.body.device_id = "Pentair: "+req.body.device_id;
    if(req.body.alias.trim().toLowerCase().endsWith('pool')) req.body.alias = req.body.alias.trim().slice(0,-4).trim();
    if(req.body.alias.trim().toLowerCase().startsWith('pool')) req.body.alias = req.body.alias.trim().slice(4).trim();

    const params = [res.locals.user._id, req.body.device_id, req.body.password];

    slApi.slLogin(...params).then(body => {
        next();
    })
    .catch(err => {
        debug('edit:error %s', JSON.stringify(err));
        return res.send(JSON.stringify({status:'error','message':'Device ID or password is wrong!'}));
    });
});

router.post('/editDevice',  function(req, res) {
    debug('edit: %s', JSON.stringify(req.body));

    pool_device.updateDevice(req.body.id, res.locals.user._id, req.body.alias, req.body.device_id,
        req.body.password, req.body.enabled==='on', function (err) {
        if (err) {
            debug('edit:errors %s', JSON.stringify(res.locals.tpl_data.errors));
            return res.send(JSON.stringify({status:'error','message':err.message}));
        } else {
            return res.send(JSON.stringify({status:'successful'}));
        }
    });
});

router.post('/delDevice',  function(req, res) {
    res.locals.tpl_data.id = req.body._id;

    debug('del: %s', JSON.stringify(req.body));

        pool_device.delDevice(res.locals.user._id, req.body.id, function (err, ret) {
            if (err) {
                return res.send(JSON.stringify({status:'error'}));
            } else {
                return res.send(JSON.stringify({status:'successful'}));
            }
        });

});



module.exports = router;
