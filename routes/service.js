const express = require('express');
const router = express.Router();
const fs = require('fs');
const config = require('../config');

router.post('/service/logs', (req, res) => {
    if (req.body.key !== config.PentairHealth.key) {
        res.statusCode = 401;
        res.end('Unauthorized');
    }

    let fileContent = fs.readFileSync('./log/err.log');

    res.send(fileContent);
});

router.post('/service/ping', (req, res) => {
    if (req.body.key !== config.PentairHealth.key) {
        res.statusCode = 401;
        res.end('Unauthorized');
    }

    res.send();
});

module.exports = router;