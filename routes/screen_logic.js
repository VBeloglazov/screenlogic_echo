'use strict';

const express = require('express');
const debug = require('debug')('screenlogic-echo:alexa-app-router');
const router = express.Router();
const errors = require('../apps/ScreenLogic/errors');

const alexaApp = require('../apps/ScreenLogic');
const cfg = require('../config');
const oauth2 = require('../model/oauth2')(cfg.lockit);
const pool_device = require('../model/device')();
const verifier = require('alexa-verifier');


// Configure GET requests to run a debugger UI
if (cfg.debug) {
    router.get('/ScreenLogic/test', (req, res) => {
        if (typeof req.query.schema !== 'undefined') {
            res.set('Content-Type', 'text/plain').send(alexaApp.schema());
        }
        else if (typeof req.query.utterances !== 'undefined') {
            res.set('Content-Type', 'text/plain').send(alexaApp.utterances());
        }
        else if (typeof req.query.intents !== 'undefined') {
            res.set('Content-Type', 'text/plain').send(alexaApp.intents);
        }
        else {
            res.render('test', { 'app': alexaApp, 'schema': alexaApp.schema(), 'utterances': alexaApp.utterances(), 'intents': alexaApp.intents });
        }
    });
}


// NOTE: !!!
// TODO: alexa-verifier git version used for last-minute changes (current released version 0.0.5)
// TODO: must be changed in package.json when new version will be available
/*
 * verify amazon request
*/
router.post('/ScreenLogic/test', (req, res, next) => {
    debug(' >> -------------------------------------------------------------------- >>');

    // test signature
    Promise.resolve().then(() => req.headers.signaturecertchainurl.length).catch(() => {
        debug('AMAZON request verification:bad headers: ');
        debug('%s', JSON.stringify(req.headers));
        throw new Error('Request not signed');
    })
    .then(() => {
        // mark the request body as already having been parsed so it's ignored by
        // other body parser middlewares
        req._body = true;
        req.rawBody = '';

        req.on('data', data => req.rawBody += data);

        req.on('end', () => {
            let er;
            try {
                req.body = JSON.parse(req.rawBody);
            } catch (error) {
                er = error;
                req.body = {};
            }
            const cert_url = req.headers.signaturecertchainurl;
            const signature = req.headers.signature;
            const requestBody = req.rawBody;
            verifier(cert_url, signature, requestBody, er => {
                if (er) {
                    console.error('error validating the alexa cert:', er);
                    res.status(401).json({ status: 'failure', reason: er });
                } else {
                    debug('AMAZON request verification: OK');
                    debug('%s', JSON.stringify(req.headers));
                    next();
                }
            });
        });
    })
    .catch(e => {
        debug(`AMAZON request verification: ${e}`);
        res.status(401).json({ status: 'failure', reason: e.message });
    });
});


/*
 * additional amazon session and user account validations
*/
router.post('/ScreenLogic/test', (req, res, next) => {
    function setAndThrowError (type, error, reason) {
        req.body.session.localError = {type, error};
        throw new Error(reason);
    }

    function validateAccessToken (accessToken) {
        debug('check:session is ok: %s', JSON.stringify(accessToken));

        // check user and devices
        return oauth2.findAccessToken(accessToken).then(data => {
            // no record mean user data lost or token expiration
            if (!data.userId) {
                debug('check:token no uid: %s', JSON.stringify(data));
                req.body.session.localStatus.notLinked = true;
                setAndThrowError('USER', 'ACCOUNT_NOT_LINKED', 'Account linkage disabled by user');
            }

            // linkage disabled by user
            if (data.disabled) {
                debug('check:linkage disabled: %s', JSON.stringify(data));
                req.body.session.localStatus.linkDisabled = true;
                setAndThrowError('USER', 'ACCOUNT_LINKAGE_DISABLED', 'Account linkage disabled by user');
            }
            return data.userId;
        })
        .then(userId => {
            if (!userId) { return null; }

            req.body.session.localStatus.localUserId = userId;

            // check devices
            return pool_device.getUserDevices(userId).then(devices => {
                debug('check:userDevices:: %s', JSON.stringify(devices));

                if (devices.length <= 0) {
                    // no devices added
                    req.body.session.localStatus.noDevice = true;
                    setAndThrowError('USER', 'ACCOUNT_NO_DEVICES', 'User have no devices added');
                }

                // check enabled devices
                for (const dev of devices) {
                    if (dev.enabled) {
                        // one or more devices enabled
                        req.body.session.localStatus.localUserDev = devices;
                        return null;
                    }
                }

                // all devices disable
                req.body.session.localStatus.noDevicesEnabled = true;
                setAndThrowError('USER', 'ACCOUNT_DEVICES_DISABLED', 'User have no devices enabled');
            });
        });
    }

    // test session
    Promise.resolve().then(() => req.body.session.user.length).catch(e => {
        debug(`check: no user data: ${e}`);
        setAndThrowError('REQUEST', 'BAD_SESSION', 'No user data provided in session');
    })
    .then(() => {
        // create variables to collect errors and path them to skill
        req.body.session.localError = null;

        // for localUserDev - array of user devices
        // and localUserId - user ID
        req.body.session.localStatus = {};
        return null;
    })
    .then(() => {
        if (!req.body.session.user.accessToken) {
            debug('check:no token: %s', JSON.stringify(req.body.session.user));
            req.body.session.localStatus.notLinked = true;
            setAndThrowError('USER', 'ACCOUNT_NOT_LINKED', 'No access token provided in session');
        }
        return req.body.session.user.accessToken;
    })
    .then(validateAccessToken)
    .then(() => {
        debug('check:complete: OK');
        next();
    })
    .catch(e => {
        debug(`check:catched: ${e}`);
        Promise.resolve().then(() => req.body.session.localError.WILL_THROW_IF_NOT_SET).then(() => {
            debug('check:catched: saved as localError and will be sent to user');
            // let skill's app.error handler handle all catched errors (request.data.session.localError)
            next();
        })
        .catch(() => {
            debug('check:catched: saved as localStatus.error general');
            req.body.session.localStatus.error = e;
            next();
        });
    });
});


/*
 * Manually hook the handler function into express
*/
router.post('/ScreenLogic/test', (req, res) => {
    debug('request: %s', JSON.stringify(req.body));

    // alexa-app returns a promise with the response
    alexaApp.request(req.body).then(response => {
        debug('response: %s', JSON.stringify(response));
        debug(' << -------------------------------------------------------------------- <<');

        // stream it to express' output
        res.json(response);
    })
    .catch(error => {
        debug(`catched: ${error}`);
        errors.logError(error);

        debug(' << -------------------------------------------------------------------- <<');
        res.json({status:'failure', reason:'Skill error'});
    });
});


module.exports = router;