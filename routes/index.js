var express = require('express');
var cfg = require('../config');
var router = express.Router();
var debug = require('debug')('screenlogic-echo:index-router');

/* pre route */
router.all('/*', function(req, res, next) {
    res.locals.tpl_data = {
        title: cfg.appname,
        isLoggedIn: res.locals.name || false
    };
    next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
    if (res.locals.name){
        res.redirect('/profile');
        //res.render('index', res.locals.tpl_data);
    } else {
       // debug('router.get("/": %s', res.locals.name);
        res.redirect('/login');
    }
    
});

/* GET about page. */
router.get('/about', function(req, res, next) {
    res.render('about', res.locals.tpl_data);
});


module.exports = router;
