var express = require('express');
var debug = require('debug')('screenlogic-echo:profile-router');
var cfg = require('../config');
var utils = require('lockit-utils');
var router = express.Router();
var pool_device = require('../model/device')();
var oauth2 = require('../model/oauth2')(cfg.lockit);
var slApi = require('../model/sl_api');

/* pre route */
router.all('/*', function(req, res, next) {
    // req.session.failedLoginAttempts=0;
    // req.session.name='tkapluk';
    // req.session.email='tkapluk@gmail.com';
    // req.session.loggedIn=true;
    // res.cookie('user_id', '1', {signed: true})
    next();});
router.all('/*', utils.restrict(cfg.lockit), function(req, res, next) {console.log(req.signedCookies);
    res.locals.tpl_data = {
        title: cfg.appname,
        isLoggedIn: res.locals.name || false
    };

    // check user linkage
    if (req.signedCookies.user_id) {
        oauth2.isUserLinked(req.signedCookies.user_id, function(err, isLinked) {
            if (err) {
                debug('pre:isUserLinked:error: %s', JSON.stringify(err));
                res.locals.tpl_data.error = 'An error occured during linkage check!';
                res.locals.tpl_data.isLinked = false;
            }
            else {
                debug('pre:isUserLinked:status %s', JSON.stringify(isLinked));
                res.locals.tpl_data.isLinked = isLinked;
                res.locals.tpl_data.isLinkEnabled = (isLinked === 2);
            }

            next();
        });
    } else {
        res.locals.tpl_data.isLinked = false;
        next();
    }
});


/* profile page. */
router.get('/', utils.restrict(cfg.lockit), (req, res) => {
    pool_device.getUserDevices(req.signedCookies.user_id).then(devices => {
        res.locals.tpl_data.devices = devices;
        res.render('profile', res.locals.tpl_data);
        return null;
    })
    .catch(err => {
        res.locals.tpl_data.error = err;
        res.render('profile', res.locals.tpl_data);
        return null;
    });
});


/* devise add page. */
router.get('/add', utils.restrict(cfg.lockit), function(req, res) {
    debug('add:user_id %s', JSON.stringify(req.signedCookies.user_id));
    res.render('device_add', res.locals.tpl_data);
});

// check device via API
router.post('/add', utils.restrict(cfg.lockit), function(req, res, next) {
    req.body.device_id = "Pentair: "+req.body.device_id;
    if(req.body.alias.trim().toLowerCase().endsWith('pool')) req.body.alias = req.body.alias.trim().slice(0,-4).trim();
    if(req.body.alias.trim().toLowerCase().startsWith('pool')) req.body.alias = req.body.alias.trim().slice(4).trim();
    debug('add:device: %s', JSON.stringify(req.body));

    const params = [req.signedCookies.user_id, req.body.device_id, req.body.password];

    slApi.slLogin(...params).then(body => {
        next();
    })
    .catch(err => {
        debug('add:error %s', JSON.stringify(err));

        res.locals.tpl_data.error = JSON.stringify(err).indexOf('offline')? 'Pool system is offline. Please check connection and try again.' : 'Device ID or password is wrong!';
        res.locals.tpl_data.alias = req.body.alias;
        res.locals.tpl_data.device_id = req.body.device_id.slice(9);
        res.locals.tpl_data.password = req.body.password;

        res.render('device_add', res.locals.tpl_data);
    });
});

router.post('/add', utils.restrict(cfg.lockit), function(req, res) {
    debug('add:user_id %s', JSON.stringify(req.signedCookies.user_id));

    pool_device.addDevice(req.signedCookies.user_id, req.body.alias, req.body.device_id, req.body.password, function (err) {
        if (err) {
            // validator
            res.locals.tpl_data.errors = {};
            if (err.errors !== undefined){
                err.errors.forEach(function (error) {
                    res.locals.tpl_data.errors[error.path] = error.message;
                });
                debug('add:errors %s', JSON.stringify(res.locals.tpl_data.errors));
            }
            res.locals.tpl_data.error = err.message;
            res.locals.tpl_data.alias = req.body.alias;
            res.locals.tpl_data.device_id = req.body.device_id.slice(9);
            res.locals.tpl_data.password = req.body.password;

            // TODO: check device status and set "enabled"

            res.render('device_add', res.locals.tpl_data);
        } else {
            res.redirect('/profile');
        }
    });
});


/* device edit page */
router.get('/edit/:id', utils.restrict(cfg.lockit), function(req, res) {
    res.locals.tpl_data.id = req.params.id;

    debug('edit:id %s', JSON.stringify(req.params.id));

    pool_device.getDeviceById(req.params.id, function (err, device) {
        if (err) {
            res.locals.tpl_data.error = err.message;
        } else {
            res.locals.tpl_data._id = device._id;
            res.locals.tpl_data.alias = device.alias;
            res.locals.tpl_data.device_id = device.device_id.slice(9);
            res.locals.tpl_data.password = device.password;
            res.locals.tpl_data.enabled = device.enabled ? 'on' : '';
        }
        res.render('device_edit', res.locals.tpl_data);
    });
});

// check device via API
router.post('/edit/:id', utils.restrict(cfg.lockit), function(req, res, next) {
    debug('edit:device: %s', JSON.stringify(req.body));
    req.body.device_id = "Pentair: "+req.body.device_id;
    if(req.body.alias.trim().toLowerCase().endsWith('pool')) req.body.alias = req.body.alias.trim().slice(0,-4).trim();
    if(req.body.alias.trim().toLowerCase().startsWith('pool')) req.body.alias = req.body.alias.trim().slice(4).trim();

    const params = [req.signedCookies.user_id, req.body.device_id, req.body.password];

    slApi.slLogin(...params).then(body => {
        next();
    })
    .catch(err => {
        debug('edit:error %s', JSON.stringify(err));

        res.locals.tpl_data.error = 'Device ID or password is wrong!';
        res.locals.tpl_data._id = req.body._id;
        res.locals.tpl_data.alias = req.body.alias;
        res.locals.tpl_data.device_id = req.body.device_id.slice(9);
        res.locals.tpl_data.password = req.body.password;
        res.locals.tpl_data.enabled = req.body.enabled;

        res.render('device_edit', res.locals.tpl_data);
    });
});

router.post('/edit/:id', utils.restrict(cfg.lockit), function(req, res) {
    res.locals.tpl_data.id = req.body._id;

    debug('edit: %s', JSON.stringify(req.body));

    pool_device.updateDevice(req.body._id, req.signedCookies.user_id, req.body.alias, req.body.device_id,
        req.body.password, req.body.enabled==='on', function (err) {
        if (err) {
            // validator
            res.locals.tpl_data.errors = {};
            err.errors.forEach(function (error) {
                res.locals.tpl_data.errors[error.path] = error.message;
            });
            debug('edit:errors %s', JSON.stringify(res.locals.tpl_data.errors));

            res.locals.tpl_data.error = err.message;
            res.locals.tpl_data._id = req.body._id;
            res.locals.tpl_data.alias = req.body.alias;
            res.locals.tpl_data.device_id = req.body.device_id.slice(9);
            res.locals.tpl_data.password = req.body.password;
            res.locals.tpl_data.enabled = req.body.enabled;

            res.render('device_edit', res.locals.tpl_data);
        } else {
            res.redirect('/profile');
        }
    });
});

/* device remove page. */
router.get('/del/:id', utils.restrict(cfg.lockit), function(req, res) {
    res.locals.tpl_data.id = req.params._id;

    debug('del:id %s', JSON.stringify(req.params.id));

    pool_device.getDeviceById(req.params.id, function (err, device) {
        if (err) {
            res.locals.tpl_data.error = err.message;
        } else {
            res.locals.tpl_data._id = device._id;
            res.locals.tpl_data.alias = device.alias;
        }
        res.render('device_del', res.locals.tpl_data);
    });
});

router.post('/del/:id', utils.restrict(cfg.lockit), function(req, res) {
    res.locals.tpl_data.id = req.body._id;

    debug('del: %s', JSON.stringify(req.body));

    if (req.body.remove === 'yes') {
        pool_device.delDevice(req.signedCookies.user_id, req.body._id, function (err, ret) {
            if (err) {
                res.locals.tpl_data.error = err.message;
                res.locals.tpl_data._id = req.body._id;
                res.locals.tpl_data.alias = req.body.alias;

                res.render('device_del', res.locals.tpl_data);
            } else {
                res.redirect('/profile');
            }
        });
    } else {
        res.redirect('/profile/edit/'+req.body._id);
    }
});

/* unlink Alexa page. */
router.get('/unlink', utils.restrict(cfg.lockit), function(req, res) {
    if (res.locals.tpl_data.isLinked) {
        oauth2.disableUserLinkage(req.signedCookies.user_id, function (err, dropped) {
            if (err) {
                debug('unlink:error: %s', JSON.stringify(err));
                res.locals.tpl_data.error = 'An error occured during linkage drop!';
            }
            else {
                debug('unlink:status %s', JSON.stringify(dropped));
                res.locals.tpl_data.success = 'Your linkage with Alexa dropped successfully.';
                res.locals.tpl_data.isLinkEnabled = false;
            }
            res.render('alexa_unlink', res.locals.tpl_data);
        });
    } else {
        debug('unlink:status: not linked');
        res.locals.tpl_data.error = 'Your currently not linked with Alexa.';
        res.locals.tpl_data.isLinked = false;
        res.render('alexa_unlink', res.locals.tpl_data);
    }
});

/* link Alexa page. */
router.get('/link', utils.restrict(cfg.lockit), function(req, res) {
    if (res.locals.tpl_data.isLinked) {
        oauth2.enableUserLinkage(req.signedCookies.user_id, function (err, dropped) {
            if (err) {
                debug('link:error: %s', JSON.stringify(err));
                res.locals.tpl_data.error = 'An error occured during linkage recovery!';
            }
            else {
                debug('link:status %s', JSON.stringify(dropped));
                res.locals.tpl_data.success = 'Your linkage with Alexa recovered successfully.';
                res.locals.tpl_data.isLinkEnabled = true;
            }
            res.render('alexa_unlink', res.locals.tpl_data);
        });
    } else {
        debug('link:status: not linked');
        res.locals.tpl_data.error = 'Your currently not linked with Alexa.';
        res.locals.tpl_data.isLinked = false;
        res.render('alexa_unlink', res.locals.tpl_data);
    }
});

module.exports = router;
