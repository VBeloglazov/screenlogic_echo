var express = require('express');
var debug = require('debug')('screenlogic-echo:message-router');
var router = express.Router();
var messages = require('../model/messages')();
var moment = require('moment');

/* pre route */
router.all('/*',  function(req, res, next) {
    
    if (req.url !== "/getMessages" && req.session.email!=="david.maccallum@pentair.com") {
        res.redirect('/login');
        return null;
    }

        next();

});


/* message page. */
router.get('/',  (req, res) => {
    messages.getMessages().then(messages => {
        res.locals.tpl_data.messages = messages;
        res.locals.tpl_data.moment = moment;
        res.render('messages', res.locals.tpl_data);
        return null;
    })
    .catch(err => {
        res.locals.tpl_data.error = err;
        res.render('messages', res.locals.tpl_data);
        return null;
    });
});


/* message for api. */
router.get('/getMessages',  (req, res) => {
    messages.getMessages().then(messages => {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(messages));
        return null;
    })
    .catch(err => {
        res.locals.tpl_data.error = err;
        res.render('messages', res.locals.tpl_data);
        return null;
    });
});


/* message  add page. */
router.get('/add',  function(req, res) {
    res.render('message_add', res.locals.tpl_data);
});


router.post('/add',  function(req, res) {
    debug('add:message %s', JSON.stringify(req.body));

    messages.addMessage( req.body.title, req.body.body, function (err) {
        if (err) {
            // validator
            res.locals.tpl_data.errors = {};
            err.errors.forEach(function (error) {
                res.locals.tpl_data.errors[error.path] = error.message;
            });
            debug('add:errors %s', JSON.stringify(res.locals.tpl_data.errors));

            res.locals.tpl_data.error = err.message;
            res.locals.tpl_data.title = req.body.title;
            res.locals.tpl_data.body = req.body.body;

            res.render('message_add', res.locals.tpl_data);
        } else {
            res.redirect('/messages');
        }
    });
});


/* message edit page */
router.get('/edit/:id',  function(req, res) {
    res.locals.tpl_data.id = req.params.id;

    debug('edit:id %s', JSON.stringify(req.params.id));

    messages.getMessageById(req.params.id, function (err, message) {
        if (err) {
            res.locals.tpl_data.error = err.message;
        } else {
            res.locals.tpl_data._id = message._id;
            res.locals.tpl_data.message_title = message.title;
            res.locals.tpl_data.message_body = message.body;
        }
        res.render('message_edit', res.locals.tpl_data);
    });
});


router.post('/edit/:id',  function(req, res) {
    res.locals.tpl_data.id = req.body._id;

    debug('edit: %s', JSON.stringify(req.body));

    messages.updateMessage(req.body._id, req.body.title, req.body.body, function (err) {
        if (err) {
            // validator
            res.locals.tpl_data.errors = {};
            err.errors.forEach(function (error) {
                res.locals.tpl_data.errors[error.path] = error.message;
            });
            debug('edit:errors %s', JSON.stringify(res.locals.tpl_data.errors));

            res.locals.tpl_data.error = err.message;
            res.locals.tpl_data._id = req.body._id;
            res.locals.tpl_data.title = req.body.title;
            res.locals.tpl_data.body = req.body.body;

            res.render('message_edit', res.locals.tpl_data);
        } else {
            res.redirect('/messages/show/'+req.body._id);
        }
    });
});

/* message remove page. */
router.get('/del/:id',  function(req, res) {
    res.locals.tpl_data.id = req.params._id;

    debug('del:id %s', JSON.stringify(req.params.id));

    messages.getMessageById(req.params.id, function (err, message) {
        if (err) {
            res.locals.tpl_data.error = err.message;
        } else {
            res.locals.tpl_data._id = message._id;
            res.locals.tpl_data.title = message.title;
        }
        res.render('message_del', res.locals.tpl_data);
    }); 
});

router.post('/del/:id',  function(req, res) {
    res.locals.tpl_data.id = req.body._id;

    debug('del: %s', JSON.stringify(req.body));

    if (req.body.remove === 'yes') {
        messages.delMessage(req.body._id, function (err, ret) {
            if (err) {
                res.locals.tpl_data.error = err.message;
                res.locals.tpl_data._id = req.body._id;
                res.locals.tpl_data.title = req.body.title;

                res.render('message_del', res.locals.tpl_data);
            } else {
                res.redirect('/messages');
            }
        });
    } else {
        res.redirect('/messages/show/'+req.body._id);
    }
});

router.get('/show/:id',  function(req, res) {
    res.locals.tpl_data.id = req.body._id;
    messages.getMessageById(req.params.id, function (err, message) {
        if (err) {
            res.locals.tpl_data.error = err.message;
        } else {
            res.locals.tpl_data._id = message._id;
            res.locals.tpl_data.message_title = message.title;
            res.locals.tpl_data.message_body = message.body;

        }
        res.render('message_show', res.locals.tpl_data);
    });

});



module.exports = router;
