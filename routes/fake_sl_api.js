'use strict';

const express = require('express');
const debug = require('debug')('screenlogic-echo:fake-api-router');
const router = express.Router();

router.all('/fakeAPI/*', (req, res, next) => {
    debug('fakeAPI:url: %s', JSON.stringify(req.url));
    debug('fakeAPI:query: %s', JSON.stringify(req.query));

    /*
    if (req.url !== '/fakeAPI/login' && !req.header('X-Session')) {
        res.json({'errorMessage':'System not registered'});
    }
    */

    // 'set' functions returns nothing
    if (req.url.match('/fakeAPI/poolSetScgConfig') || req.url.match('/fakeAPI/poolSetHeatSp')
        || req.url.match('/fakeAPI/poolButtonPress') || req.url.match('/fakeAPI/poolSetHeatMode')) {

        res.json({});
    } else {
        next();
    }
});


/*
 * login
 */
router.post('/fakeAPI/login', (req, res) => {
    debug('fakeAPI:login: %s', JSON.stringify(req.body));

    /*
    let body = {'errorMessage':'Cannot connect \'fakeDeviceName\': \'login\''};

    if (req.query.loginName && req.query.pass) {
        body = {sessionId: 999};
    }
    */

    res.json({sessionId: 999});
});


/*
 * poolData
 */
router.get('/fakeAPI/poolData', (req, res) => {
    const body = {
        'poolData': {
            'name': 'Pentair: E9-F4-DB',
            'version': 'POOL: 5.2 Build 733.0 Rel',
            'zip': 'H4Y1H1',
            'latitude': 34,
            'longitude': 119,
            'timeZone': -8,
            'weatherType': 1,
            'weatherURL': ''
        }
    };

    res.json(body);
});


/*
 * poolStatus
 */
router.get('/fakeAPI/poolStatus', (req, res) => {

    const body = {
        'poolStatus': {
            'status': 1,
            'freezeMode': 0,
            'remotes': 32,
            'poolDelay': 0,
            'spaDelay': 0,
            'cleanerDelay': 0,
            'airTemp': 67,
            'bodiesOfWaterCount': 2,
            'bodies': [
                {'type': 0, 'temp': 89, 'heatAct': 0, 'sp': 92, 'coolSp': 100, 'heatMode': 1},
                {'type': 1, 'temp': 81, 'heatAct': 0, 'sp': 94, 'coolSp': 67, 'heatMode': 0}
            ],
            'circuitsCount': 19,
            'circuits': [
                {'idStart': 500, 'onOff': 1, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 501, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 502, 'onOff': 1, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 504, 'onOff': 0, 'colorSet': 0, 'colorPos': 4, 'colorStagger': 10, 'delay': 0},
                {'idStart': 505, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 510, 'onOff': 0, 'colorSet': 4, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 511, 'onOff': 0, 'colorSet': 2, 'colorPos': 1, 'colorStagger': 10, 'delay': 0},
                {'idStart': 512, 'onOff': 0, 'colorSet': 3, 'colorPos': 2, 'colorStagger': 10, 'delay': 0},
                {'idStart': 513, 'onOff': 0, 'colorSet': 5, 'colorPos': 3, 'colorStagger': 10, 'delay': 0},
                {'idStart': 514, 'onOff': 0, 'colorSet': 0, 'colorPos': 4, 'colorStagger': 10, 'delay': 0},
                {'idStart': 518, 'onOff': 0, 'colorSet': 6, 'colorPos': 5, 'colorStagger': 10, 'delay': 0},
                {'idStart': 519, 'onOff': 1, 'colorSet': 0, 'colorPos': 6, 'colorStagger': 0, 'delay': 0},
                {'idStart': 540, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 541, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 542, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 543, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 544, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 545, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0},
                {'idStart': 546, 'onOff': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'delay': 0}
            ],
            'ph': 751,
            'orp': 748,
            'saturation': 3,
            'saltPPM': 0,
            'phTank': 6,
            'orpTank': 0,
            'alarms': 0
        }
    };

    res.json(body);
});


/*
 * poolCtrlConfig
 */
router.get('/fakeAPI/poolCtrlConfig', (req, res) => {
    const body = {
        'poolCtrlConfig': {
            'thisInterfaceId': 100,
            'minPoolSetPoint': 40,
            'maxPoolSetPoint': 104,
            'minSpaSetPoint': 40,
            'maxSpaSetPoint': 104,
            'isDegC': 0,
            'ctlrType': 2,
            'hwType': 0,
            'stat9': 96,
            'equipFlags': 4294935033,
            'genCircuitName': 'Water Features',
            'circuitsCount': 19,
            'circuitConfigs': [
                {'idStart': 500, 'name': 'Spa', 'nameIndex': 71, 'function': 1, 'interface': 1, 'flags': 1, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 1, 'defaultRunTime': 180},
                {'idStart': 501, 'name': 'Air Blower', 'nameIndex': 1, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 2, 'defaultRunTime': 45},
                {'idStart': 502, 'name': 'Chlorinator', 'nameIndex': 20, 'function': 0, 'interface': 0, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 3, 'defaultRunTime': 240},
                {'idStart': 504, 'name': 'Solar Temp', 'nameIndex': 92, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 4, 'colorStagger': 10, 'circuitIndex': 5, 'defaultRunTime': 3},
                {'idStart': 505, 'name': 'Pool', 'nameIndex': 60, 'function': 2, 'interface': 0, 'flags': 1, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 6, 'defaultRunTime': 300},
                {'idStart': 510, 'name': 'Spa Light', 'nameIndex': 73, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 4, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 11, 'defaultRunTime': 300},
                {'idStart': 511, 'name': 'Pool Light1', 'nameIndex': 93, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 2, 'colorPos': 1, 'colorStagger': 10, 'circuitIndex': 12, 'defaultRunTime': 300},
                {'idStart': 512, 'name': 'Pool Light2', 'nameIndex': 94, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 3, 'colorPos': 2, 'colorStagger': 10, 'circuitIndex': 13, 'defaultRunTime': 300},
                {'idStart': 513, 'name': 'Pool Light3', 'nameIndex': 95, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 5, 'colorPos': 3, 'colorStagger': 10, 'circuitIndex': 14, 'defaultRunTime': 300},
                {'idStart': 514, 'name': 'Yard Lights', 'nameIndex': 99, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 0, 'colorPos': 4, 'colorStagger': 10, 'circuitIndex': 15, 'defaultRunTime': 300},
                {'idStart': 518, 'name': 'Laminars', 'nameIndex': 100, 'function': 16, 'interface': 3, 'flags': 0, 'colorSet': 6, 'colorPos': 5, 'colorStagger': 10, 'circuitIndex': 19, 'defaultRunTime': 180},
                {'idStart': 519, 'name': 'Cave Light', 'nameIndex': 98, 'function': 16, 'interface': 4, 'flags': 0, 'colorSet': 0, 'colorPos': 6, 'colorStagger': 0, 'circuitIndex': 20, 'defaultRunTime': 45},
                {'idStart': 540, 'name': 'Jets', 'nameIndex': 45, 'function': 0, 'interface': 1, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 41, 'defaultRunTime': 45},
                {'idStart': 541, 'name': 'Cavefall', 'nameIndex': 96, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 42, 'defaultRunTime': 180},
                {'idStart': 542, 'name': 'Waterfall', 'nameIndex': 85, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 43, 'defaultRunTime': 180},
                {'idStart': 543, 'name': 'Slide', 'nameIndex': 69, 'function': 13, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 44, 'defaultRunTime': 120},
                {'idStart': 544, 'name': 'Pool Lights', 'nameIndex': 101, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 45, 'defaultRunTime': 210},
                {'idStart': 545, 'name': 'Spillway', 'nameIndex': 78, 'function': 14, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 46, 'defaultRunTime': 15},
                {'idStart': 546, 'name': 'Prime', 'nameIndex': 97, 'function': 0, 'interface': 2, 'flags': 0, 'colorSet': 0, 'colorPos': 0, 'colorStagger': 0, 'circuitIndex': 47, 'defaultRunTime': 1}
            ],
            'colorCount': 8,
            'colorSets': [
                {'name': 'White', 'r': 255, 'g': 255, 'b': 255},
                {'name': 'Light Green', 'r': 160, 'g': 255, 'b': 160},
                {'name': 'Green', 'r': 0, 'g': 255, 'b': 80},
                {'name': 'Cyan', 'r': 0, 'g': 255, 'b': 200},
                {'name': 'Blue', 'r': 100, 'g': 140, 'b': 255},
                {'name': 'Lavender', 'r': 230, 'g': 130, 'b': 255},
                {'name': 'Magenta', 'r': 255, 'g': 0, 'b': 128},
                {'name': 'Light Magenta', 'r': 255, 'g': 180, 'b': 210}
            ],
            'pumpAssignmentCount': 8,
            'pumpAssignment': 'qaqrk0YAAAA=',
            'poolTabAll': 127,
            'showAlarms': 1
        }
    };

    res.json(body);
});

/*
 * getChemData
 */
router.get('/fakeAPI/poolChemData', (req, res) => {
    const body = {'chemData': 'AALbAscC2gK8AAAAAgAAAAAABQAABwD6AUUAIQBzTABTAACdMDwBAAAA'};

    res.json(body);
});

/*
 * getSaltConfig
 */
router.get('/fakeAPI/poolGetScgConfig', (req, res) => {
    const body = {
        poolScgConfig : {
            flags : 128,
            isInstalled : 1,
            isStatusOk : 128,
            level1 : 70,
            level2 : 70,
            salt : 66,
            super : 0
            }
    };

    res.json(body);
});



module.exports = router;
