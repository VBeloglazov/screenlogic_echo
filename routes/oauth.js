
var express = require('express');
var oauthServer = require('oauth2-server');
var router = express.Router();
var cfg = require('../config');
var debug = require('debug')('screenlogic-echo:oauth');


// Add OAuth server.

var oauth = oauthServer({
    model: require('../model/oauth2')(cfg.lockit),
    grants: ['authorization_code', 'refresh_token'],
    debug: console.log
});


// Post token.
router.all('/oauth/token', oauth.grant());
//app.all('/login', app.oauth.autorize(), function(req, res, next) {next();});

// Show them the "do you authorise xyz app to access your content?" page
router.get('/oauth/authorise', function (req, res, next) {
    debug('logged in: %s', JSON.stringify(res.locals));
    if (!req.session.loggedIn || !req.signedCookies.user_id) {
        // If they aren't logged in, send them to your own login implementation
        return res.redirect('/login?redirect=' + encodeURIComponent(req.path
            + '?client_id=' + req.query.client_id
            + '&redirect_uri=' + req.query.redirect_uri
            + '&state=' + req.query.state
            + '&response_type=' + req.query.response_type));
    }

    //if (!req.session.user && res.locals.user) {
        req.session.user = res.locals.user;

    //}
    debug('user: %s / %s / %s', JSON.stringify(req.session), JSON.stringify(res.locals), JSON.stringify(req.signedCookies));

    res.render('authorise', {
        title: cfg.appname,
        isLoggedIn: res.locals.name || false,
        client_id: req.query.client_id,
        state: req.query.state,
        response_type: req.query.response_type,
        redirect_uri: req.query.redirect_uri
    });
});

// Handle authorise
router.post('/oauth/authorise', function (req, res, next) {
    debug('logged in: %s', JSON.stringify(res.locals));
    if (!req.session.loggedIn || !req.signedCookies.user_id) {
        return res.redirect('/login?redirect=' + encodeURIComponent(req.path
            + '?client_id=' + req.query.client_id
            + '&state=' + req.query.state
            + '&response_type=' + req.query.response_type
            + '&redirect_uri=' + req.query.redirect_uri));
    }

    //if (!req.session.user && res.locals.user) {
     //   req.session.user = res.locals.user;
    //}
    debug('user: %s / %s / %s', JSON.stringify(req.session), JSON.stringify(res.locals), JSON.stringify(req.signedCookies));

    next();
}, oauth.authCodeGrant(function (req, next) {
    debug('auth: %s', JSON.stringify(req.body));
    // The first param should to indicate an error
    // The second param should a bool to indicate if the user did authorise the app
    // The third param should for the user/uid (only used for passing to saveAuthCode)

    console.log('-- grant req: ', req.signedCookies);
    var user = {id: req.signedCookies.user_id || null};
    //next(null, req.body.allow === 'yes', req.session.user.id, req.session.user);
    next(null, req.body.allow === 'yes', user.id, user);
}));


// TODO: check this
router.all(oauth.errorHandler());

module.exports = router;