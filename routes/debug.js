/* global Promise */

const express = require('express');
const router = express.Router();
//const debug = require('debug')('screenlogic-echo:debug-route');
//const slApi = require('../model/sl_api');

// DEBUG: remove this
// DEBUG: remove this
// DEBUG: force code changed and restart app

/* GET tests. */
/*
router.get('/', (req, res) => {
    debug('call login');
    slApi.slLogin(2, 'Pentair: 00-00-00', '').then(body => {
        debug('login:response: %s', JSON.stringify(body));
        return Promise.all([
            slApi.slGetChemData(2, body.sessionId),
            slApi.slGetPoolData(2, body.sessionId)
        ]);
    })
    .then(data => {
        debug('got data: %s', JSON.stringify(data));
        res.json({data});
        return 'all ok';
    })
    .then(els => {
        debug('got els: %s', JSON.stringify(els));
    })
    .catch(err => {
        debug('slLogin error: %s', JSON.stringify(err));
        res.json({err});
    });
});
*/

/*
router.get('/', function(req, res) {

    var ret = {};

    ret.login = {};
    debug('call login');
    slApi.login(2, 'Pentair: E9-F4-DB', null, function (err, body) {
        if (err) {
            ret.login.error = err;
            res.json(ret);
        }

        ret.login.body = body;
        if (body.sessionId) {

            ret.getPoolData = {};
            debug('call getPoolData');
            slApi.getPoolData(2, body.sessionId, function (pool_err, pool_data) {
                if (pool_err) {
                    ret.getPoolData.error = pool_err;
                    res.json(ret);
                } else {
                    ret.getPoolData.body = pool_data;

                    ret.getPoolConfigData = {};
                    debug('call getPoolConfigData');
                    slApi.getPoolConfigData(2, body.sessionId, function (poolc_err, poolc_data) {
                        if (poolc_err) {
                            ret.getPoolConfigData.error = poolc_err;
                            res.json(ret);
                        } else {
                            ret.getPoolConfigData.body = poolc_data;

                            ret.getPoolsStatus = {};
                            debug('call getPoolsStatus');
                            slApi.getPoolsStatus(2, body.sessionId, function (pools_err, pools_data) {
                                if (pools_err) {
                                    ret.getPoolsStatus.error = pools_err;
                                    res.json(ret);
                                } else {
                                    ret.getPoolsStatus.body = pools_data;

                                    ret.getChemData = {};
                                    debug('call getChemData');
                                    slApi.getChemData(2, body.sessionId, function (poolch_err, poolch_data) {
                                        if (poolch_err) {
                                            ret.getChemData.error = poolch_err;
                                            res.json(ret);
                                        } else {
                                            ret.getChemData.body = poolch_data;

                                            res.json(ret);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            res.json(ret);
        }
    });

});
*/

module.exports = router;


// var ret = {
//     "login": {"body": {"sessionId": 21}},
//     "getPoolData": {
//         "body": {
//             "poolData": {
//                 "name": "Pentair: E9-F4-DB",
//                 "version": "POOL: 5.2 Build 733.0 Rel",
//                 "zip": "H4Y1H1",
//                 "latitude": 34,
//                 "longitude": 119,
//                 "timeZone": -8,
//                 "weatherType": 1,
//                 "weatherURL": ""
//             }
//         }
//     },
//     "getPoolConfigData": {
//         "body": {
//             "poolCtrlConfig": {
//                 "thisInterfaceId": 100,
//                 "minPoolSetPoint": 40,
//                 "maxPoolSetPoint": 104,
//                 "minSpaSetPoint": 40,
//                 "maxSpaSetPoint": 104,
//                 "isDegC": 0,
//                 "ctlrType": 13,
//                 "hwType": 0,
//                 "stat9": 32,
//                 "equipFlags": 4294934564,
//                 "genCircuitName": "Water Features",
//                 "circuitsCount": 18,
//                 "circuitConfigs": [{
//                     "idStart": 500,
//                     "name": "Spa",
//                     "nameIndex": 71,
//                     "function": 1,
//                     "interface": 1,
//                     "flags": 1,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 1,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 501,
//                     "name": "Aux 1",
//                     "nameIndex": 2,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 2,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 502,
//                     "name": "Aux 2",
//                     "nameIndex": 3,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 3,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 503,
//                     "name": "Aux 3",
//                     "nameIndex": 4,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 4,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 504,
//                     "name": "Aux 4",
//                     "nameIndex": 5,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 5,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 505,
//                     "name": "Pool",
//                     "nameIndex": 60,
//                     "function": 2,
//                     "interface": 0,
//                     "flags": 1,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 6,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 506,
//                     "name": "Aux 5",
//                     "nameIndex": 6,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 7,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 507,
//                     "name": "Aux 6",
//                     "nameIndex": 7,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 8,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 508,
//                     "name": "Aux 7",
//                     "nameIndex": 8,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 9,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 510,
//                     "name": "Feature 1",
//                     "nameIndex": 93,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 11,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 511,
//                     "name": "Feature 2",
//                     "nameIndex": 94,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 12,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 512,
//                     "name": "Feature 3",
//                     "nameIndex": 95,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 13,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 513,
//                     "name": "Feature 4",
//                     "nameIndex": 96,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 14,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 514,
//                     "name": "Feature 5",
//                     "nameIndex": 97,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 15,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 515,
//                     "name": "Feature 6",
//                     "nameIndex": 98,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 16,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 516,
//                     "name": "Feature 7",
//                     "nameIndex": 99,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 17,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 517,
//                     "name": "Feature 8",
//                     "nameIndex": 100,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 18,
//                     "defaultRunTime": 720
//                 }, {
//                     "idStart": 519,
//                     "name": "AuxEx",
//                     "nameIndex": 92,
//                     "function": 0,
//                     "interface": 2,
//                     "flags": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "circuitIndex": 20,
//                     "defaultRunTime": 720
//                 }],
//                 "colorCount": 8,
//                 "colorSets": [{"name": "White", "r": 255, "g": 255, "b": 255}, {
//                     "name": "Light Green",
//                     "r": 160,
//                     "g": 255,
//                     "b": 160
//                 }, {"name": "Green", "r": 0, "g": 255, "b": 80}, {
//                     "name": "Cyan",
//                     "r": 0,
//                     "g": 255,
//                     "b": 200
//                 }, {"name": "Blue", "r": 100, "g": 140, "b": 255}, {
//                     "name": "Lavender",
//                     "r": 230,
//                     "g": 130,
//                     "b": 255
//                 }, {"name": "Magenta", "r": 255, "g": 0, "b": 128}, {
//                     "name": "Light Magenta",
//                     "r": 255,
//                     "g": 180,
//                     "b": 210
//                 }],
//                 "pumpAssignmentCount": 8,
//                 "pumpAssignment": "AAYAAAAAAAA=",
//                 "poolTabAll": 127,
//                 "showAlarms": 0
//             }
//         }
//     },
//     "getPoolsStatus": {
//         "body": {
//             "poolStatus": {
//                 "status": 1,
//                 "freezeMode": 0,
//                 "remotes": 32,
//                 "poolDelay": 0,
//                 "spaDelay": 0,
//                 "cleanerDelay": 0,
//                 "airTemp": 78,
//                 "bodiesOfWaterCount": 2,
//                 "bodies": [{"type": 0, "temp": 78, "heatAct": 0, "sp": 78, "coolSp": 100, "heatMode": 0}, {
//                     "type": 1,
//                     "temp": 79,
//                     "heatAct": 0,
//                     "sp": 100,
//                     "coolSp": 78,
//                     "heatMode": 0
//                 }],
//                 "circuitsCount": 18,
//                 "circuits": [{
//                     "idStart": 500,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 501,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 502,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 503,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 504,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 505,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 506,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 507,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 508,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 510,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 511,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 512,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 513,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 514,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 515,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 516,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {
//                     "idStart": 517,
//                     "onOff": 0,
//                     "colorSet": 0,
//                     "colorPos": 0,
//                     "colorStagger": 0,
//                     "delay": 0
//                 }, {"idStart": 519, "onOff": 0, "colorSet": 0, "colorPos": 0, "colorStagger": 0, "delay": 0}],
//                 "ph": 0,
//                 "orp": 0,
//                 "saturation": 0,
//                 "saltPPM": 0,
//                 "phTank": 0,
//                 "orpTank": 0,
//                 "alarms": 1
//             }
//         }
//     },
//     "getChemData": {"body": {"chemData": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAA"}}
// };