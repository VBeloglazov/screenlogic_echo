var config = require('./config.global');
//var logger = require('bunyan');

config.env = 'development';
config.debug = true;
config.log = 'combined';

config.applicationId = [
    // 'amzn1.ask.skill.4038cb64-774e-4d02-92ca-adf4f0170904',
    // 'amzn1.echo-sdk-ams.app.28cc38a1-8b32-4756-b5ac-478ff7cf51ae',
    // 'amzn1.ask.skill.285a800e-6fbb-4cc5-815d-61697900b529',
    // 'amzn1.ask.skill.6efa83f8-39fa-461c-a4e4-d8e3ff4576cd',
    'amzn1.ask.skill.92caf8eb-c92f-4e2e-bcdd-1207201c509d',
];

config.hostname = 'https://pentairpoolchat.com';

config.mysql.user = 'itworksi_slecho';
config.mysql.database = 'itworksi_slecho';
config.mysql.password = 'mXUh85QSRbEmHZ2E';

config.lockit.url = config.hostname;
config.lockit.db.url = 'mysql://'+ config.mysql.user +':'+ config.mysql.password +'@localhost:3306/';
config.lockit.db.name = config.mysql.database;


config.lockit.emailType = 'nodemailer-smtp-transport';
config.lockit.emailFrom = 'support@pentairpoolchat.com';

config.lockit.emailSettings = {
    service: false,
    auth: false,
    ignoreTLS: true,
    transport: 'smtp',
    host: 'pentairpoolchat.com',
    debug: true,
    logger: true //logger.createLogger({name: '--smtp'})
};

module.exports = config;
