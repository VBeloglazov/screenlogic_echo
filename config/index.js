var env = process.env.SLCONF || 'dev'
  , cfg = require('./config.'+env);

module.exports = cfg;
