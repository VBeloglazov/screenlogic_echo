var config = module.exports = {};

config.env = 'development';
config.debug = false;
config.log = 'combined';

config.hostname = 'http://localhost';
config.port = '3001';

config.appname = 'ScreenLogic';
config.applicationId = [
    // 'amzn1.ask.skill.285a800e-6fbb-4cc5-815d-61697900b529',
    'amzn1.ask.skill.92caf8eb-c92f-4e2e-bcdd-1207201c509d'
];

// Pentair Health key
config.PentairHealth = {};
config.PentairHealth.key = '$$$PentairHealth2020$$$';

/* Database */
config.mysql = {};
config.mysql.host = 'localhost';
config.mysql.database = 'slecho';
config.mysql.user = 'user';
config.mysql.password = 'pass';

/* ScreenLogic API*/
config.slapi = {};
config.slapi.host = 'https://screenlogicweb.pentair.com:1005';
config.slapi.appVersion = 'Screenlogic Alexa';


/* Single sign-on */
config.sso = {};
config.sso.host = 'https://pentairpoolsso.com';
config.sso.fakePassword = 'VtzefWNBOuVi1ejMblVS';

/* Single sign-on */
config.monitor = {};
config.monitor.host = 'https://remotemonitor.intellicenter.com';

// secret
config.cookie_secret = 'd61a35e5f82402b3018f792c7dfa4cdc';


/* Lockit settings */
config.lockit = require('./lockit.cfg');

// db settings
config.lockit.db = {};
config.lockit.db.url = 'mysql://'+ config.mysql.user +':'+ config.mysql.password +'@localhost:3306/';
config.lockit.db.name = config.mysql.database;
config.lockit.db.collection = 'sl_users';

// name for subject and email content
config.lockit.appname = config.appname;

// url for proper link generation
config.lockit.url = config.hostname;


// show warning after three failed login attempts
config.lockit.failedLoginsWarning = 3;
// lock account after five failed login attempts
config.lockit.failedLoginAttempts = 5;
// lock account for 20 minutes
config.lockit.accountLockedTime = '20 minutes';

// !Important: We use login process for Echo Acconut Linking
// normal login -> /profile/
// linking -> aws_redirect_url
//config.lockit.login.handleResponse = false;


// custom signup page w/ password confirmation
config.lockit.signup.views.signup = 'signup.jade';
// custom forgot-password page w/ password confirmation
config.lockit.forgotPassword.views.newPassword = 'get-new-password.jade';

// public email address of your app
config.lockit.emailFrom = 'welcome@example.com';

// email settings (same as nodemailer)
config.lockit.emailTemplate = 'lockit-template-blank';
config.lockit.emailType = 'nodemailer-stub-transport';

config.lockit.emailSettings = {
    service: 'none',
    auth: {
        user: 'none',
        pass: 'none'
    }
};

