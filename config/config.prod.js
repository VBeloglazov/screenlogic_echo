var config = require('./config.global');

config.env = 'production';
config.debug = false;

config.hostname = 'prod.example.com';

config.mysql.host = 'mysql.example.com';
config.mysql.user = 'mysql_user';
config.mysql.password = 'mysql_password';

config.lockit.db.url = 'mysql://'+ config.mysql.user +':'+ config.mysql.password +'@localhost:3306/';


module.exports = config;
