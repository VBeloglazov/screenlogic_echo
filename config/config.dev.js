var config = require('./config.global');
//var logger = require('bunyan');

config.env = 'development';
config.debug = true;
config.log = 'dev';

config.applicationId = [
    'fake-one',
    'amzn1.echo-sdk-ams.app.28cc38a1-8b32-4756-b5ac-478ff7cf51ae',
    'fake-last'
];

config.hostname = 'http://slecho.local:3001';

//config.mysql.socketPath = '/tmp/alexa-mysql.socket';
config.mysql.user = 'root';
config.mysql.password = 'password';

// fake sl API host
//config.slapi.host = 'http://slecho.local:8088/fakeAPI';

config.lockit.url = config.hostname;

//MySQL
//config.lockit.db.url = 'mysql://'+ config.mysql.user +':'+ config.mysql.password +'@localhost:3306/';

// SQLite
config.lockit.db.url = 'sqlite://';
config.lockit.db.name = config.lockit.db.name + '.db';
//config.lockit.db = {
//   url: 'sqlite://',
//   name: ':memory:',
//   collection: 'my_user_table'
//};


config.lockit.emailType = 'nodemailer-stub-transport';
config.lockit.emailFrom = 'support@localhost';

/*
config.lockit.emailSettings = {
    service: false,
    auth: false,
    ignoreTLS: true,
    transport: 'smtp',
    host: 'pentairpoolchat.com',
    debug: true,
    logger: true //logger.createLogger({name: '--smtp'})
};
*/

/*
config.lockit.emailSettings = {
    host: 'inlisu.com',
    ignoreTLS: true,
    secured: false,
    debug: true,
    logger: logger.createLogger({name: '--smtp'})
};
*/

/* Single sign-on */
config.monitor = {};
config.monitor.host = 'http://slmonitor.tim.inlisu.com';
module.exports = config;
